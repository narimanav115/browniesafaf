import 'package:brownies/data/blocs/login/bloc/auth_bloc.dart';
import 'package:brownies/data/blocs/products/bloc.dart';
import 'package:brownies/data/providers/cities_provider.dart';
import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:brownies/services/push_service.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/screens/cart_screen.dart';
import 'package:brownies/ui/screens/home_page.dart';
import 'package:brownies/ui/screens/order_final_screen.dart';
import 'package:brownies/ui/screens/profile/about_us.dart';
import 'package:brownies/ui/screens/profile/authorization.dart';
import 'package:brownies/ui/screens/profile/bonus_info.dart';
import 'package:brownies/ui/screens/profile/delivery_info.dart';
import 'package:brownies/ui/screens/profile/registration.dart';
import 'package:brownies/ui/screens/profile/restoration.dart';
import 'package:brownies/ui/screens/profile/settings.dart';
import 'package:brownies/ui/screens/profile_page.dart';
import 'package:brownies/ui/screens/splash_login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';

final CitiesProvider cityProvider = CitiesProvider();

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  registerOnFirebase();
  runApp(MyApp(
    authenticationRepository: AuthenticationRepository(),
    citiesProvider: cityProvider,
  ));
}

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

class MyApp extends StatelessWidget {
  const MyApp({
    Key key,
    @required this.authenticationRepository,
    @required this.citiesProvider,
  })  : assert(authenticationRepository != null),
        assert(citiesProvider != null),
        super(key: key);
  final AuthenticationRepository authenticationRepository;
  final CitiesProvider citiesProvider;

  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: authenticationRepository,
      child: BlocProvider(
        create: (_) => AuthenticationBloc(
          authenticationRepository: authenticationRepository,
        ),
        child: ChangeNotifierProvider(
          create: (_) => ProductsControllerUI(),
          child: MultiProvider(
            providers: [
              ChangeNotifierProvider.value(
                value: citiesProvider,
              ),
            ],
            child: MultiBlocProvider(
              providers: [
                BlocProvider.value(value: ProductsBloc(citiesProvider)),
                BlocProvider.value(
                    value: LoginBloc(
                  authenticationRepository: authenticationRepository,
                ))
              ],
              child: Builder(builder: (BuildContext context) {
                print("widgets rebuilded");
                return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  routes: {
                    RegisterScreen.id: (context) => RegisterScreen(),
                    SplashScreenLogin.id: (context) => SplashScreenLogin(),
                    ProfilePage.id: (context) => ProfilePage(),
                    ForgotPassword.id: (context) => ForgotPassword(),
                    AuthorizationScreen.id: (context) => AuthorizationScreen(),
                    HomePage.id: (context) => HomePage(),
                    DeliveryInfo.id: (context) => DeliveryInfo(),
                    BonusInfo.id: (context) => BonusInfo(),
                    Settings.id: (context) => Settings(),
                    CartScreen.id: (context) => CartScreen(),
                    OrderPage.id: (context) => OrderPage(),
                    AboutUs.id: (context) => AboutUs(),
                    //NewPassword.id: (context) => NewPassword(),
                  },
                  theme: ThemeData(
                      appBarTheme: AppBarTheme(
                          iconTheme: IconThemeData(color: purpleBrownies)),
                      buttonColor: purpleBrownies,
                      primaryColor: purpleBrownies,
                      accentColor: purpleBrownies),
                  home: SplashScreenLogin(),
                );
              }),
            ),
          ),
        ),
      ),
    );
  }
}
