import 'dart:async';
import 'dart:convert';

import 'package:brownies/configs/url.dart';
import 'package:brownies/main.dart';
import 'package:brownies/repository/models/user_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

enum AuthenticationStatus {
  unknown,
  authenticated,
  unauthenticated,
  error,
  edit
}

class AuthenticationRepository {
  final _controller = StreamController<AuthenticationStatus>();
  User _user;
  String _error = ' ';

  Stream<AuthenticationStatus> get status async* {
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  logOut() async {
    _controller.add(AuthenticationStatus.unauthenticated);
    _user = null;
    return null;
  }

  reset({@required String sendTo}) async {
    assert(sendTo != null);

    try {
      final request = await http.put(Uri.parse(url + 'reset-password'), headers: {
        'Accept': 'application/json',
      }, body: {
        'email': sendTo,
      });

      var data = jsonDecode(request.body);
      print(data);
    } catch (err) {
      print('$err');
      _error = err.toString();
    }
  }

  String getAddress() {
    if (_user != null && _user != User.empty)
      return _user.address;
    else
      return null;
  }

  String getPorch() {
    if (_user != null && _user != User.empty)
      return _user.porch;
    else
      return null;
  }

  String getApartment() {
    if (_user != null && _user != User.empty)
      return _user.apartment;
    else
      return null;
  }

  String getToken() {
    if (_user != null && _user != User.empty)
      return _user.token;
    else
      return null;
  }

  Future<void> register(
      {@required String phone,
      @required String password,
      @required String email}) async {
    assert(phone != null);
    assert(password != null);
    print('$phone $password $email');
    try {
      final request = await http.post(Uri.parse(url + 'auth/register'), headers: {
        'Accept': 'application/json',
      }, body: {
        'phone': phone,
        'password': password,
        'email': email,
        'token_name': ' sadsa'
      });

      var data = jsonDecode(request.body);
      print(data);
      if (data['token'] != null) {
        await logIn(phone: phone, password: password);
        return data['token'];
      } else {
        _error = request.body;
        print('elrse');
        _controller.add(AuthenticationStatus.error);

        return null;
      }
    } catch (err) {
      print('catch');
      _controller.add(AuthenticationStatus.error);

      _error = err.toString();
      return null;
    }
  }

  Future<User> edit(body) async {
    _controller.add(AuthenticationStatus.edit);

    print(body);
    try {
      final request = await http.post(Uri.parse(url + 'edit'),
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ${_user.token}',
          },
          body: body);

      var data = jsonDecode(request.body);
      print(data);
      if (request.statusCode == 200) {
        _user = User(
            token: _user.token,
            phone: _user.phone,
            name: data['name'],
            balance: data['data'],
            bonus: data['bonus'],
            city: data['city'],
            address: data['address'],
            porch: data['porch'],
            bday: data['bday'],
            apartment: data['apartment']);
        _controller.add(AuthenticationStatus.authenticated);
        return _user;
      } else {
        _error = data['message'];
        return null;
      }
    } catch (err) {
      print(err);
      return null;
    }
  }

  Future<String> getError() async {
    return _error;
  }

  Future<User> logIn({
    String phone,
    String password,
  }) async {
    if (_user != null) return _user;
    assert(phone != null);
    assert(password != null);

    try {
      final request = await http.post(Uri.parse(url + 'auth/login'), headers: {
        'Accept': 'application/json',
      }, body: {
        'phone': phone,
        'password': password,
        'token_name': ' sadsa'
      });

      var data = jsonDecode(request.body);
      print(data);
      if (request.statusCode == 200) {
        _user = User.fromJson(data);
        try {
          cityProvider.chosenCity = cityProvider.cities
              .firstWhere((element) => element.city.contains(_user.city));
        } catch (err) {
          cityProvider.chosenCity = cityProvider.cities.first;
        }
        _controller.add(AuthenticationStatus.authenticated);
        print('returnin from login');
        return _user;
      } else {
        _error = data['message'];
        _controller.add(AuthenticationStatus.error);

        await Future.delayed(
          Duration(milliseconds: 300),
        );
        _controller.add(AuthenticationStatus.unauthenticated);

        return null;
      }
    } catch (err) {
      _error = err.toString();
      _controller.add(AuthenticationStatus.error);
      return null;
    }
  }

  void dispose() => _controller.close();
}
