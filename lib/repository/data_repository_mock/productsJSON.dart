var categoriesJson = {
  "categories": [
    {
      "title": "Новинки",
      "id": 1,
      "products": [
        {
          "id": 1,
          "title": "Карамельный торт",
          "category": "Новинки",
          "img": "assets/pie.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        },
        {
          "id": 2,
          "title": "Карамельный торт",
          "category": "Новинки",
          "img": "assets/pie2.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        },
        {
          "id": 3,
          "title": "Карамельный торт",
          "category": "Новинки",
          "img": "assets/pie3.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        },
        {
          "id": 4,
          "title": "Карамельный торт",
          "category": "Новинки",
          "img": "assets/pie4.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": false,
          "liquid": false
        }
      ]
    },
    {
      "title": "Комбо",
      "id": 2,
      "products": [
        {
          "id": 1,
          "title": "Карамельный торт",
          "category": "Комбо",
          "img": "assets/pie.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        },
        {
          "id": 2,
          "title": "Карамельный торт",
          "category": "Комбо",
          "img": "assets/pie2.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        },
        {
          "id": 3,
          "title": "Карамельный торт",
          "category": "Комбо",
          "img": "assets/pie3.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        }
      ]
    },
    {
      "title": "Еда",
      "id": 3,
      "products": [
        {
          "id": 1,
          "title": "Карамельный торт",
          "category": "Еда",
          "img": "assets/pie.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        },
        {
          "id": 2,
          "title": "Карамельный торт",
          "category": "Еда",
          "img": "assets/pie2.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        }
      ]
    },
    {
      "title": "Холодные закуски",
      "id": 4,
      "products": [
        {
          "id": 1,
          "title": "Карамельный торт",
          "category": "Холодные закуски",
          "img": "assets/pie.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        },
        {
          "id": 2,
          "title": "Карамельный торт",
          "category": "Холодные закуски",
          "img": "assets/pie2.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        }
      ]
    },
    {
      "title": "Холодныwqrwq",
      "id": 5,
      "products": [
        {
          "id": 1,
          "title": "Карамельный торт",
          "category": "Холодныwqrwq",
          "img": "assets/pie.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        },
        {
          "id": 2,
          "title": "Карамельный торт",
          "category": "Холодныwqrwq",
          "img": "assets/pie2.png",
          "components": [
            {
              "id": 1,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 2,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 3,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 4,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 5,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 6,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 7,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 8,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 9,
              "component-name": "Somen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 10,
              "component-name": "Omen",
              "image": "assets/component2.png",
              "added": true
            },
            {
              "id": 11,
              "component-name": "Domen",
              "image": "assets/component1.png",
              "added": true
            },
            {
              "id": 12,
              "component-name": "Women",
              "image": "assets/component2.png",
              "added": true
            }
          ],
          "price": 750,
          "in-stock": true,
          "liquid": false
        }
      ]
    }
  ],
  "banners":[
    {
      "title":"banner1",
      "image":"assets/pag1.png"
    },
    {
      "title":"banner2",
      "image":"assets/pag2.png"
    }
  ]
};
