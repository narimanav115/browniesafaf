import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String token;
  final String phone;
  final String name;
  final int balance;
  final int bonus;
  final String city;
  final String address;
  final String porch;
  final String apartment;
  final String bday;

  const User(
      {this.token,
      this.phone,
      this.name,
      this.balance,
      this.bonus,
      this.city,
      this.address,
      this.porch,
      this.bday,
      this.apartment});

  User.fromJson(Map<String, dynamic> json)
      : token = json['token'],
        phone = json['phone'],
        name = json['name'],
        balance = json['balance'],
        bonus = json['bonus'],
        city = json['city'],
        address = json['address'],
        porch = json['porch'],
        bday = json['bday'],
        apartment = json['apartment'];

  User copyWith(
      {String token,
      String phone,
      String name,
      int balance,
      int bonus,
      String city,
      String address,
      String porch,
      String bday,
      String apartment}) {
    return User(
      name: name ?? this.name,
      phone: phone ?? this.phone,
      balance: balance ?? this.balance,
      bonus: bonus ?? this.bonus,
      city: city ?? this.city,
      address: address ?? this.address,
      porch: porch ?? this.porch,
      bday: bday ?? this.bday,
      apartment: apartment ?? this.apartment,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['phone'] = this.phone;
    data['name'] = this.name;
    data['balance'] = this.balance;
    data['bonus'] = this.bonus;
    data['city'] = this.city;
    data['address'] = this.address;
    data['porch'] = this.porch;
    data['bday'] = this.bday;
    data['apartment'] = this.apartment;
    return data;
  }

  @override
  List<Object> get props =>
      [token, name, balance, bonus, city, address, porch, apartment, bday];

  static const empty = User(token: '-');
}
