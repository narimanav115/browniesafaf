class City {
  int id;
  String city;
  List<Address> address;

  City({this.id, this.city, this.address});

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    city = json['city'];
    if (json['address'] != null) {
      address = [];
      json['address'].forEach((v) {
        address.add(new Address.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['city'] = this.city;
    if (this.address != null) {
      data['address'] = this.address.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Address {
  int id;
  int cityId;
  String address;

  Address({this.id, this.cityId, this.address});
  toString() {
    return 'id: $id, address: $address, city id:$cityId';
  }

  Address.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    cityId = json['city_id'];
    address = json['address'];
    print(address);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['city_id'] = this.cityId;
    data['address'] = this.address;
    return data;
  }
}
