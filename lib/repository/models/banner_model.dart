class BannerList {
  List<PickedBanner> banners = [];

  BannerList({this.banners});

  BannerList.fromJson(Map<String, dynamic> json) {
    if (json['banner'] != null) {
      json['banner'].forEach((v) {
        print(v);
        banners.add(new PickedBanner.fromJson(v));
      });
    }
  }
}

class PickedBanner {
  String title;
  String image;

  PickedBanner({this.title, this.image});

  toString() {
    return 'url:$title, img:$image';
  }

  PickedBanner.fromJson(Map<String, dynamic> json) {
    title = json['link'];
    image = json['img'];
  }
}
