import 'package:brownies/configs/url.dart';
import 'package:brownies/repository/models/cart_product.dart';

class Categories {
  List<Category> categories;

  Categories({this.categories});

  Categories.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      categories = [];
      json['data'].forEach((v) {
        categories.add(new Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.categories != null) {
      data['data'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Category {
  int id;
  Null parentId;
  double offset = 0;
  int order;
  String title;
  String slug;
  String createdAt;
  String updatedAt;
  List<Product> productList;

  Category(
      {this.id,
      this.parentId,
      this.order,
      this.title,
      this.slug,
      this.createdAt,
      this.updatedAt,
      this.productList});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parentId = json['parent_id'];
    order = json['order'];
    title = json['name'];
    slug = json['slug'];
    if (json['products'] != null) {
      productList = [];
      json['products'].forEach((v) {
        productList.add(new Product.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['parent_id'] = this.parentId;
    data['order'] = this.order;
    data['name'] = this.title;
    data['slug'] = this.slug;
    if (this.productList != null) {
      data['products'] = this.productList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Product {
  int id;
  String title;
  String description;
  String img;
  int price;
  int percent;
  int inStock;
  int fluid;
  int categoryId;
  List<Components> components = [];
  String componentsString = '';
  List<Variations> variations = [];

  Variations variation;
  int quantity = 1;

  Product(
      {this.id,
      this.title,
      this.description,
      this.img,
      this.price,
      this.percent,
      this.inStock,
      this.fluid,
      this.categoryId});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    img = imgDomain + json['img'];
    price = json['price'];
    percent = json['percent'];
    inStock = json['in_stock'];
    fluid = json['fluid'];
    categoryId = json['category_id'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    //data['title'] = this.title;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    if (this.components != null) {
      data['components'] = this.components.map((v) => v.toJson()).toList();
    }
    data['variation'] = this.variation;
    return data;
  }

  toString() {
    return '$id, $title, $price, $percent%, in stock: $inStock, is fluid: $fluid,\n $variations:$variation\ncomponents:$components';
  }
}
