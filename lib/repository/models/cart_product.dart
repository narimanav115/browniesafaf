import 'package:brownies/configs/url.dart';

class ComponentList {
  List<Components> components;

  ComponentList({this.components});

  ComponentList.fromJson(Map<String, dynamic> json) {
    if (json['true_component'] != null) {
      components = [];
      json['true_component'].forEach((v) {
        components.add(new Components.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.components != null) {
      data['true_component'] = this.components.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Components {
  int id;
  String componentName;
  int price;
  String image;

  toString() {
    return 'Component id:$id, name:$componentName, price:$price';
  }

  Components({this.id, this.componentName, this.image, this.price});

  Components.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    componentName = json['component-name'];
    price = json['price'];
    image = imgDomain + json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    return data;
  }
}

class Variations {
  int id;
  String name;
  String price;

  toString() {
    return 'Variation: id:$id, name:$name, price:$price';
  }

  Variations({this.id, this.name, this.price});

  Variations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    return data;
  }
}
