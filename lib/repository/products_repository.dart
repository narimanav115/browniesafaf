import 'dart:convert';

import 'package:brownies/configs/url.dart';
import 'package:brownies/repository/models/banner_model.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:http/http.dart' as http;

class ProductRepository {
  List<Product> allProducts = []; //Во всех кате
  BannerList banners; // гориях

  Future<List<Category>> fetchCategories({int id}) async {
    allProducts.clear(); //Во всех кате
    //int a =1 ;
    final request = await http.post(Uri.parse(url + 'category/category'), headers: {
      "Accept": "application/json",
      // "Authorization": "Bearer 15|Mt0vwWmOOEp2ZRU7vF2QScycVSjaRhM3yjdACvVY",
    }, //Categories.fromJson()
        body: {
          "city": id.toString()
        });
    var response = json.decode(request.body);

    Categories data = Categories.fromJson(response);
    banners = BannerList.fromJson(response);

    data.categories[0].offset = 300.00;
    //begin hardcode for offsets
    /*
    Здесь добавляется хардкод, который высчитывает оффсет для одного row
    чтобы при скролле вычислять на главном меню на какой категории находится
    оффсет. Там есть скроллистенер, который высчитывает оффсет и соотносит с офсетом
    который высчитан здесь
     */
    for (int a = 0; a < data.categories.length; a++) {
      int tile = (data.categories[a].productList.length % 2 +
              data.categories[a].productList.length) ~/
          2;
      if (a < data.categories.length - 1)
        data.categories[a + 1].offset =
            data.categories[a].offset + tile * 260 + 30 / (a + 1);
      allProducts += data.categories[a].productList;
    }
    //end hardcode for offsets

    return data.categories;
  }
}
