import 'dart:convert';

import 'package:achievement_view/achievement_view.dart';
import 'package:brownies/repository/models/cart_product.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProductsControllerUI extends ChangeNotifier {
  List<Product> cartItems = [];
  int tabIndex = 0;
  List<Components> components = [];
  List<Components> testComp = [];
  List<Variations> variations = [];
  Map orderDetails = {
    "payment_type_id": 1.toString(),
    "payment_status_id": 1.toString(),
    "name": "'qwdwqd'",
    "delivery_type_id": 1.toString(),
    "delivery_status_id": 1.toString(),
    "asap": true.toString(),
    "comments": "'qwdqwdwq'",
    "porch": "'asd'",
    "apartment": '51',
    "address": "Shevchenko 165b",
    // "Authorization" : "Bearer 15|Mt0vwWmOOEp2ZRU7vF2QScycVSjaRhM3yjdACvVY"
  };
  int sum = 0;
  int deliveryPrice = 300;
  getPriceOfProduct(Product product) {
    int temp = 0;
    testComp.forEach((element) {
      temp += element.price;
    });
    return temp + product.price;
  }

  resetComponents() {
    testComp.clear();
    notifyListeners();
  }

  // @override
  ProductsControllerUI();

  void changeDeliverMap(key, value) {
    orderDetails[key] = value.toString();
    print(orderDetails);
    // notifyListeners();
  }

  changeComponents(value, component) {
    if (value == true) {
      testComp.add(component);
    } else {
      testComp.remove(component);
    }
    print(testComp.length);
    notifyListeners();
  }

  Future fetchComponents(id, context) async {
    testComp.clear();
    components.clear();
    variations.clear();
    print(id);
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            content: Center(child: CircularProgressIndicator.adaptive()),
          );
        });
    final request = await http.get(
      Uri.parse('https://brownies.a-lux.dev/api/category/products/$id'),
      headers: {
        "Accept": "json/application",
        "Authorization": "Bearer 15|Mt0vwWmOOEp2ZRU7vF2QScycVSjaRhM3yjdACvVY"
      }, //Categories.fromJson()
    );
    var response = json.decode(request.body);

    response["data"]['true_component'].forEach((v) {
      components.add(new Components.fromJson(v));
    });
    response["data"]['variations'].forEach((v) {
      variations.add(new Variations.fromJson(v));
    });
    Navigator.pop(context);
    return components;
  }

  order(basket, context, token) async {
    orderDetails['basket'] = basket;
    print(orderDetails);
    final request = await http.post(
      Uri.parse('https://brownies.a-lux.dev/api/order'),
      headers: {
        "Accept": "json/application",
        "Authorization": "Bearer $token"
      }, //Categories.fromJson()
      body: orderDetails,
      //encoding: Encoding.getByName("utf-8")
    );
    var response = json.decode(request.body);
    print(response);
    cartItems.clear();
    notifyListeners();
    Navigator.pop(context);
    showAlert(context, 'Спасибо!', 'Ваш заказ принят', false);
  }

  // ignore: missing_return
  Future<int> binarySearch(
      List<Category> cats, offset, int min, int max, preLast) async {
    int mid = (max + min) ~/ 2;
    if (min + 1 == max) {
      if (tabIndex != min && offset <= cats.last.offset) {
        tabIndex = min;
        return min;
      }
      if (tabIndex != max && offset >= cats.last.offset) {
        tabIndex = max;
        return preLast;
      }
    } else if (offset >= cats[mid].offset)
      await binarySearch(cats, offset, mid, max, preLast);
    else
      await binarySearch(cats, offset, min, mid, preLast);
  }

  addToCart(Product product) {
    if (!cartItems.contains(product)) {
      // product.components = [];
      cartItems.add(product);
    } else {
      cartItems[cartItems.indexOf(product)].quantity++;
    }
    notifyListeners();
  }

  Future clearCart() async {
    cartItems.forEach((element) {
      element.quantity = 1;
    });
    cartItems.clear();
    sum = 0;
    notifyListeners();
  }

  int summaryAmount() {
    int summary = 0;
    for (var e in cartItems) {
      summary += e.quantity * e.price;
    }
    sum = summary;
    return sum;
  }

  int productCount() {
    int q = 0;
    for (var e in cartItems) {
      q += e.quantity;
    }
    return q;
  }

  incrementByIndex(index) {
    cartItems[index].quantity++;
    notifyListeners();
  }

  updater() {
    notifyListeners();
  }

  decrement(index) {
    cartItems[index].quantity > 1
        ? cartItems[index].quantity--
        : cartItems.removeAt(index);
    notifyListeners();
  }

  void showAlert(BuildContext context, mainText, subText, error) {
    AchievementView(context,
        title: mainText,
        subTitle: subText,
        //onTab: _onTabAchievement,
        icon: Icon(
          error ? Icons.error : Icons.check,
          color: Theme.of(context).primaryColor,
        ),
        color: Colors.white,
        textStyleTitle: TextStyle(color: Theme.of(context).primaryColor),
        textStyleSubTitle:
            TextStyle(fontSize: 12, color: Theme.of(context).primaryColor),
        duration: Duration(seconds: 1, milliseconds: 300),
        isCircle: true,
        listener: (status) {})
      ..show();
  }
}
