import 'dart:convert';

import 'package:brownies/configs/url.dart';
import 'package:brownies/data/blocs/products/bloc.dart';
import 'package:brownies/data/blocs/products/events.dart';
import 'package:brownies/repository/models/cities_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class CitiesProvider extends ChangeNotifier {
  City chosenCity;
  List<City> cities = [];

  CitiesProvider() {
    getCities();
  }

  getCities() async {
    cities = [];
    try {
      final request = await http.get(Uri.parse(url + 'cities'), headers: {
        'Accept': 'application/json',
      });

      var data = jsonDecode(request.body);

      if (request.statusCode == 200) {
        for (var t in data['data']) {
          cities.add(City.fromJson(t));
          //cities.add(t['city']);
        }
        if (chosenCity == null) chosenCity = cities.first;
      } else {
        print('Something went wrong in request');
      }
    } catch (err) {
      print('Failed fetch data from server: ' + err);
    }
  }

  changeCity(newValue, context, ProductsBloc bloc) {
    chosenCity = cities.firstWhere((element) => element.city == newValue);
    bloc.add(ProductCityChangeEvent());
    notifyListeners();
  }
}
