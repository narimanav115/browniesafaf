import 'package:brownies/data/blocs/products/events.dart';
import 'package:brownies/data/blocs/products/states.dart';
import 'package:brownies/data/providers/cities_provider.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:brownies/repository/products_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductRepository api = ProductRepository();
  final CitiesProvider citiesProvider;

  List<Category> categories;

  ProductsBloc(this.citiesProvider) : super(ProductsInitialState());

  @override
  Stream<ProductsState> mapEventToState(ProductsEvent event) async* {
    if (event is ProductLoadEvent) {
      yield ProductsLoadingState();
      try {
        categories =
            await api.fetchCategories(id: citiesProvider.cities.first.id);
        yield ProductsLoadedState(
            loadedCategories: categories,
            banners: api.banners.banners,
            products: api.allProducts);
      } catch (_) {
        print(_);
        yield ProductsErrorState();
      }
    } else if (event is ProductCityChangeEvent) {
      yield ProductsLoadingState();
      try {
        categories =
            await api.fetchCategories(id: citiesProvider.chosenCity.id);
        yield ProductsLoadedState(
            loadedCategories: categories,
            banners: api.banners.banners,
            products: api.allProducts);
      } catch (_) {
        print(_);
        yield ProductsErrorState();
      }
    } else if (event is ProductClearEvent) {
      yield ProductsEmptyState();
    } else if (event is ProductInitialEvent) {
      print('initial');
      yield ProductsInitialState();
    }
  }
}
