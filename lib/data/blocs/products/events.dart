abstract class ProductsEvent {}

class ProductInitialEvent extends ProductsEvent {}

class ProductLoadEvent extends ProductsEvent {}

class ProductCityChangeEvent extends ProductsEvent {}

class ProductClearEvent extends ProductsEvent {}