import 'package:brownies/repository/models/banner_model.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:flutter/material.dart';

abstract class ProductsState {}

class ProductsInitialState extends ProductsState {}

class ProductsEmptyState extends ProductsState {}

class ProductsLoadingState extends ProductsState {}

class ProductsShimmerState extends ProductsState {
  List<Category> loadedCategories;
  List<PickedBanner> banners;
  List products;

  ProductsShimmerState(
      {@required this.loadedCategories, this.banners, this.products})
      : assert(loadedCategories != null);
}

class ProductsLoadedState extends ProductsState {
  List<Category> loadedCategories;
  List<PickedBanner> banners;
  List products;

  ProductsLoadedState(
      {@required this.loadedCategories, this.banners, this.products})
      : assert(loadedCategories != null);
}

class ProductsErrorState extends ProductsState {}
