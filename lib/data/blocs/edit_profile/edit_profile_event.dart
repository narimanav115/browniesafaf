part of 'edit_profile_bloc.dart';

abstract class EditProfileEvent {
  const EditProfileEvent();
}

class EditSubmitted extends EditProfileEvent {
  const EditSubmitted();
}

class EditSubmitDone extends EditProfileEvent {
  const EditSubmitDone(this.body);

  final body;
}
