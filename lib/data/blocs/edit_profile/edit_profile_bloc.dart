import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:brownies/repository/models/user_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'edit_profile_event.dart';
part 'edit_profile_state.dart';

class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  EditProfileBloc({
    @required AuthenticationRepository authenticationRepository,
  })  : assert(authenticationRepository != null),
        _authenticationRepository = authenticationRepository,
        super(EditProfileState());

  final AuthenticationRepository _authenticationRepository;

  @override
  Stream<EditProfileState> mapEventToState(
    EditProfileEvent event,
  ) async* {
    if (event is EditSubmitDone) {
      yield* _submit(event, event.body);
    }
  }

  Stream<EditProfileState> _submit(EditProfileEvent event, map) async* {
    try {
      final User temp = await _authenticationRepository.edit(map);
      yield state.copyWith(newUser: temp);
    } on Exception catch (_) {
      print('temp');
    }
  }
}
