part of 'edit_profile_bloc.dart';

class EditProfileState extends Equatable {
  const EditProfileState({
    this.initialUser = User.empty,
  });

  final User initialUser;

  EditProfileState copyWith({
    User newUser,
  }) {
    return EditProfileState(
      initialUser: newUser ?? this.initialUser,
    );
  }

  @override
  List<Object> get props => [initialUser];
}


