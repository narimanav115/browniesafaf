import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:brownies/data/blocs/login/validation/password.dart';
import 'package:brownies/data/blocs/login/validation/username.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:meta/meta.dart';

part 'registration_event.dart';
part 'registration_state.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  RegistrationBloc({
    @required AuthenticationRepository authenticationRepository,
  })  : assert(authenticationRepository != null),
        _authenticationRepository = authenticationRepository,
        super(const RegistrationState());

  final AuthenticationRepository _authenticationRepository;

  @override
  Stream<RegistrationState> mapEventToState(
    RegistrationEvent event,
  ) async* {
    if (event is RegLoginUsernameChanged) {
      yield _mapUsernameChangedToState(event, state);
    } else if (event is RegLoginPasswordChanged) {
      yield _mapPasswordChangedToState(event, state);
    } else if (event is RegLoginSubmitted) {
      yield* _mapLoginSubmittedToState(event, state);
    }
  }

  RegistrationState _mapUsernameChangedToState(
    RegLoginUsernameChanged event,
    RegistrationState state,
  ) {
    final username = Username.dirty(event.username);
    return state.copyWith(
      username: username,
      status: Formz.validate([state.password, username]),
    );
  }

  RegistrationState _mapPasswordChangedToState(
    RegLoginPasswordChanged event,
    RegistrationState state,
  ) {
    final password = Password.dirty(event.password);
    return state.copyWith(
      password: password,
      status: Formz.validate([password, state.username]),
    );
  }

  Stream<RegistrationState> _mapLoginSubmittedToState(
    RegLoginSubmitted event,
    RegistrationState state,
  ) async* {
    if (state.status.isValidated) {
      yield state.copyWith(status: FormzStatus.submissionInProgress);
      try {
        await _authenticationRepository.register(
            phone: state.username.value,
            password: state.password.value,
            email: event.email);
        yield state.copyWith(status: FormzStatus.submissionSuccess);
      } on Exception catch (_) {
        yield state.copyWith(status: FormzStatus.submissionFailure);
      }
    }
  }
}
