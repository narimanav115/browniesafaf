part of 'registration_bloc.dart';

abstract class RegistrationEvent extends Equatable {
  const RegistrationEvent();
  @override
  List<Object> get props => [];
}

class RegLoginUsernameChanged extends RegistrationEvent {
  const RegLoginUsernameChanged(this.username);

  final String username;

  @override
  List<Object> get props => [username];
}

class RegLoginPasswordChanged extends RegistrationEvent {
  const RegLoginPasswordChanged(this.password);

  final String password;

  @override
  List<Object> get props => [password];
}

class RegLoginSubmitted extends RegistrationEvent {
  final String email;
  const RegLoginSubmitted({this.email});
}
