part of 'registration_bloc.dart';

class RegistrationState extends Equatable {
  const RegistrationState(
      {this.status = FormzStatus.pure,
      this.username = const Username.pure(),
      this.password = const Password.pure(),
      this.email});

  final FormzStatus status;
  final Username username;
  final Password password;
  final String email;

  RegistrationState copyWith({
    FormzStatus status,
    Username username,
    Password password,
  }) {
    return RegistrationState(
      status: status ?? this.status,
      username: username ?? this.username,
      password: password ?? this.password,
    );
  }

  @override
  List<Object> get props => [status, username, password, email];
}
