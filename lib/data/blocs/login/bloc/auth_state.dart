part of 'auth_bloc.dart';


class AuthenticationState extends Equatable {
  const AuthenticationState._({
    this.status = AuthenticationStatus.unknown,
    this.user = User.empty,
    this.error
  });

  const AuthenticationState.unknown() : this._();

  const AuthenticationState.authenticated(User _user)
      : this._(status: AuthenticationStatus.authenticated, user: _user);

  const AuthenticationState.edit(User _user)
      : this._(status: AuthenticationStatus.authenticated, user: _user);

  const AuthenticationState.error(String errorState)
      : this._(status: AuthenticationStatus.error, error: errorState);

  const AuthenticationState.unauthenticated()
      : this._(status: AuthenticationStatus.unauthenticated, user: User.empty);

  final AuthenticationStatus status;
  final User user;
  final String error;

  @override
  List<Object> get props => [status, user];


}

class LoginState extends Equatable {
  const LoginState({
    this.status = FormzStatus.pure,
    this.username = const Username.pure(),
    this.password = const Password.pure(),
  });

  final FormzStatus status;
  final Username username;
  final Password password;

  LoginState copyWith({
    FormzStatus status,
    Username username,
    Password password,
  }) {
    return LoginState(
      status: status ?? this.status,
      username: username ?? this.username,
      password: password ?? this.password,
    );
  }

  @override
  List<Object> get props => [status, username, password];
}

