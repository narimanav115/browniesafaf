import 'package:equatable/equatable.dart';

class ErrorModel extends Equatable{
  final String error;

  const ErrorModel({this.error});

  ErrorModel.fromJson(Map<String, dynamic> json) :
        error = json['error'];


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    return data;
  }

  @override
  List<Object> get props => [error];

  static const empty = ErrorModel(error: '-');
}