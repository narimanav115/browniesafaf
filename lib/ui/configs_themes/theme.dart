import 'package:flutter/material.dart';

Map<int, Color> color = {
  50: Color.fromRGBO(136, 14, 79, .1),
  100: Color.fromRGBO(136, 14, 79, .2),
  200: Color.fromRGBO(136, 14, 79, .3),
  300: Color.fromRGBO(136, 14, 79, .4),
  400: Color.fromRGBO(136, 14, 79, .5),
  500: Color.fromRGBO(136, 14, 79, .6),
  600: Color.fromRGBO(136, 14, 79, .7),
  700: Color.fromRGBO(136, 14, 79, .8),
  800: Color.fromRGBO(136, 14, 79, .9),
  900: Color.fromRGBO(136, 14, 79, 1),
};

MaterialColor purpleBrownies = MaterialColor(0xffFE00AE, color);
MaterialColor blueBrownies = MaterialColor(0xff0056FE, color);
Color grey = Color(0xffCCCCCC);

var backgroundDecor = BoxDecoration(
  image: DecorationImage(
    image: AssetImage(
      "assets/background.png",
    ),
    fit: BoxFit.cover,
  ),
);

var decorationContainer = BoxDecoration(
  borderRadius: BorderRadius.only(
    topLeft: Radius.circular(15),
    topRight: Radius.circular(15),
    bottomLeft: Radius.circular(15),
    bottomRight: Radius.circular(15),
  ),
  boxShadow: [
    BoxShadow(
        color: Color.fromRGBO(0, 0, 0, 0.25),
        offset: Offset(4, 4),
        blurRadius: 4)
  ],
  color: Color.fromRGBO(255, 255, 255, 1),
);
var decorPurple = BoxDecoration(
  borderRadius: BorderRadius.only(
    topLeft: Radius.circular(15),
    topRight: Radius.circular(15),
    bottomLeft: Radius.circular(15),
    bottomRight: Radius.circular(15),
  ),
  boxShadow: [
    BoxShadow(
        color: Color.fromRGBO(0, 0, 0, 0.25),
        offset: Offset(4, 4),
        blurRadius: 4)
  ],
  color: Color.fromRGBO(254, 0, 174, 0.6600000262260437),
);
var textStyleBlue = TextStyle(
    color: Color.fromRGBO(38, 107, 241, 1),
    fontFamily: 'Open Sans',
    fontSize: 15,
    letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
    fontWeight: FontWeight.normal,
    height: 1);
var textStyle = TextStyle(
    color: Color.fromRGBO(0, 0, 0, 1),
    fontFamily: 'Open Sans',
    fontSize: 15,
    letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
    fontWeight: FontWeight.normal,
    height: 1);
