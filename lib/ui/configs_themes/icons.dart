import 'package:flutter/widgets.dart';

class Brownies {
  Brownies._();

  static const String _fontFamily = 'icomoon';

  static const IconData back_arrow = IconData(0xe900, fontFamily: _fontFamily);
  static const IconData double_card = IconData(0xe903, fontFamily: _fontFamily);
  static const IconData inst = IconData(0xe904, fontFamily: _fontFamily);
  static const IconData menu = IconData(0xe905, fontFamily: _fontFamily);
  static const IconData pie = IconData(0xe906, fontFamily: _fontFamily);
  static const IconData profile = IconData(0xe907, fontFamily: _fontFamily);
  static const IconData shopping_cartC =
      IconData(0xe90a, fontFamily: _fontFamily);
}
