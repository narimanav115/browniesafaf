import 'dart:convert';

import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/screens/order_final_screen.dart';
import 'package:brownies/ui/widgets/appbar/buttons/bar_elements.dart';
import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  static var id = 'cartScreen';
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    // var w = MediaQuery.of(context).size.width;
    var cartProvider = Provider.of<ProductsControllerUI>(context);
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
        backgroundColor: CupertinoColors.white,
        leading: LeadingButton(),
        title: Text(
          'Корзина',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: GestureDetector(
              onTap: () {
                if (cartProvider.cartItems.length != 0) {
                  showDialog(
                    context: context,
                    builder: (_) => NetworkGiffyDialog(
                      image: Image.network(
                          "https://raw.githubusercontent.com/Shashank02051997/FancyGifDialog-Android/master/GIF's/gif14.gif"),
                      title: Text('Очистить корзину?',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 22.0, fontWeight: FontWeight.w600)),
                      description: Text(
                        'Вы собираетесь очистить корзину с вашими добавленными товарами, Вы уверены, что хотите это сделать?',
                        textAlign: TextAlign.center,
                      ),
                      entryAnimation: EntryAnimation.DEFAULT,
                      onOkButtonPressed: () {
                        cartProvider.clearCart();
                        Navigator.of(context).pop();
                      },
                    ),
                  );
                } else {
                  cartProvider.showAlert(
                      context, 'Корзина пуста', 'Нечего удалять', true);
                }
              },
              child: Text(
                'Очистить',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 16,
              ),
              Container(
                padding: EdgeInsets.fromLTRB(16, 0, 0, 0),
                child: Text(
                  'Заказ на сумму: ${cartProvider.summaryAmount()}',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                ),
              ),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  print(cartProvider.cartItems);

                  if (cartProvider.cartItems.isNotEmpty)
                    return CartItem(cartProvider.cartItems[i], i);
                  else {
                    return Center(
                      child: Text('Корзина пуста'),
                    );
                  }
                },
                itemCount: cartProvider.cartItems.length,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        margin: const EdgeInsets.all(36.0),
        color: Colors.transparent,
        child: ActionButton(
          enable: true,
          trigger: true,
          text: 'Оформить заказ',
          onTap: () {
            String temp = jsonEncode(cartProvider.cartItems);
            print(temp);
            // cartProvider.order(temp);

            // if(cartProvider.cartItems.isNotEmpty){
            Navigator.pushNamed(context, OrderPage.id);
            //
            // }else{
            //   cartProvider.topAlert(context, 'Корзина пуста', 'Добавьте продукты для заказа', true);
            // }
          },
        ),
      ),
    );
  }
}

class CartItem extends StatefulWidget {
  final Product cartItem;
  final index;
  CartItem(this.cartItem, this.index);

  @override
  _CartItemState createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  @override
  Widget build(BuildContext context) {
    var mainTextStyle = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 14,
    );
    // ignore: close_sinks
    var bloc = Provider.of<ProductsControllerUI>(context);
    var w = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Container(
        width: w - 32,
        height: 140,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 16,
              child: Padding(
                padding: const EdgeInsets.only(right: 16, left: 16),
                child: Container(
                  width: w - 64,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.cartItem.title ?? '',
                        style: mainTextStyle,
                      ),
                      Text(
                        (widget.cartItem.price * widget.cartItem.quantity)
                                .toString() ??
                            '',
                        style: mainTextStyle,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              left: 16,
              top: 52,
              child: Container(
                height: 76,
                width: 96,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(widget.cartItem.img),
                      fit: BoxFit.fitHeight),
                ),
              ),
            ),
            Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  padding: EdgeInsets.only(right: 16, bottom: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          bloc.incrementByIndex(widget.index);
                        },
                        child: Container(
                          width: 32,
                          height: 32,
                          decoration: ShapeDecoration(
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  width: 1.0,
                                  style: BorderStyle.solid,
                                  color: blueBrownies),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(32)),
                            ),
                          ),
                          child: Center(
                              child: Icon(
                            Icons.add,
                            color: Colors.black,
                          )),
                        ),
                      ),
                      Container(
                        width: 48,
                        height: 32,
                        child: Center(
                            child: Text(
                          widget.cartItem.quantity.toString() ?? '',
                          style: TextStyle(color: Colors.black, fontSize: 20),
                        )),
                      ),
                      GestureDetector(
                        onTap: () {
                          bloc.decrement(widget.index);
                        },
                        child: Container(
                          width: 32,
                          height: 32,
                          decoration: ShapeDecoration(
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  width: 1.0,
                                  style: BorderStyle.solid,
                                  color: blueBrownies),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(32)),
                            ),
                          ),
                          child: Center(
                              child: Icon(
                            Icons.remove,
                            color: Colors.black,
                          )),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
