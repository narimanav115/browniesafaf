import 'package:brownies/data/blocs/login/bloc/auth_bloc.dart';
import 'package:brownies/data/blocs/products/bloc.dart';
import 'package:brownies/data/blocs/products/events.dart';
import 'package:brownies/data/blocs/products/states.dart';
import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/screens/cart_screen.dart';
import 'package:brownies/ui/screens/profile_page.dart';
import 'package:brownies/ui/screens/splash_login.dart';
import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:brownies/ui/widgets/sticky_header_with_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  static var id = 'home';
  @override
  Widget build(BuildContext context) {
    var status = BlocProvider.of<AuthenticationBloc>(context).state.status;
    return Scaffold(
        extendBody: true,
        appBar: AppBar(
          leading: !(status == AuthenticationStatus.authenticated)
              ? IconButton(
                  icon: Icon(
                    Icons.login,
                    color: blueBrownies,
                  ),
                  onPressed: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SplashScreenLogin()),
                        (route) => false);
                  },
                )
              : IconButton(
                  icon: Icon(
                    Icons.menu_outlined,
                    color: blueBrownies,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, ProfilePage.id);
                  },
                ),
          title: Image.asset(
            'assets/title.png',
            scale: 4,
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          // actions: [
          //   Padding(
          //     padding: EdgeInsets.only(right: 14, top: 2),
          //     child: Center(
          //       child: GestureDetector(
          //         onTap: (){
          //           Navigator.pushNamed(context, CartScreen.id);
          //         },
          //         child: Builder(
          //           builder: (context) {
          //             var cartProvider = Provider.of<ProductsControllerUI>(context, listen: false);
          //             return Badge(
          //                 badgeColor: purpleBrownies,
          //                 badgeContent: Text(cartProvider.cartItems.length.toString(),
          //                   style: TextStyle(color: Colors.white),),
          //                 child: Container(
          //                   width: 35,
          //                     height: 35,
          //                     child: Image.asset('assets/cart.png')));
          //           }
          //         ),
          //       ),
          //     ),
          //   )
          // ],
        ),
        body: BlocBuilder<ProductsBloc, ProductsState>(
          builder: (context, state) {
            if (state is ProductsLoadedState) {
              return HomePageBody(state: state);
            }
            if (state is ProductsErrorState) {
              return Center(
                child: Text(
                  'Ошибка сети, повторите позднее',
                  style: TextStyle(fontSize: 20.0),
                ),
              );
            }
            if (state is ProductsInitialState) {
              BlocProvider.of<ProductsBloc>(context).add(ProductLoadEvent());
              return Center(child: CircularProgressIndicator());
            }
            if (state is ProductsLoadingState) {
              return Center(child: CircularProgressIndicator());
            }
            return Center(
              child: Text(
                'Ошибка сервера, повторите позднее',
                style: TextStyle(fontSize: 20.0),
              ),
            );
          },
        ),
        bottomNavigationBar: CartButton());
  }
}

class CartButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cartItemProvider = Provider.of<ProductsControllerUI>(context);
    int a = cartItemProvider.productCount();

    String end = '';
    if (a > 1 && a < 5)
      end = 'а';
    else if (a > 4) end = 'ов';

    return Container(
      margin: const EdgeInsets.all(36.0),
      color: Colors.transparent,
      child: a == 0
          ? Container(
              width: 0,
              height: 0,
            )
          : ActionButton(
              enable: true,
              trigger: true,
              text: '${cartItemProvider.productCount()} товар' + end,
              onTap: () {
                Navigator.pushNamed(context, CartScreen.id);
                //
                // }else{
                //   cartProvider.topAlert(context, 'Корзина пуста', 'Добавьте продукты для заказа', true);
                // }
              },
            ),
    );
  }
}
