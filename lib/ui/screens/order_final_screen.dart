import 'dart:convert';
import 'dart:core';

import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:brownies/ui/widgets/appbar/buttons/bar_elements.dart';
import 'package:brownies/ui/widgets/text_field_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class OrderPage extends StatefulWidget {
  static var id = 'orderPage';

  @override
  OrderPageState createState() => OrderPageState();
}

class OrderPageState extends State<OrderPage> with TickerProviderStateMixin {
  List<Widget> tabs;
  TabController _controllerTab;
  TextEditingController timeCtl;
  String comment;
  bool asap = false;
  bool isCash = false;
  bool useBonus = false;
  bool address = false;

  TextEditingController addressField = TextEditingController();
  TextEditingController appartmentField = TextEditingController();
  TextEditingController entranceField = TextEditingController();

  @override
  void initState() {
    super.initState();
    timeCtl = TextEditingController(
        text: DateTime.now().hour.toString() +
            ':' +
            DateTime.now().minute.toString());
    _controllerTab = TabController(length: 2, vsync: this);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      addressField.text =
          RepositoryProvider.of<AuthenticationRepository>(context).getAddress();

      appartmentField.text =
          RepositoryProvider.of<AuthenticationRepository>(context)
              .getApartment();

      entranceField.text =
          RepositoryProvider.of<AuthenticationRepository>(context).getPorch();
    });
  }

  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    ProductsControllerUI loginProvide =
        Provider.of<ProductsControllerUI>(context);
    tabs = [
      Container(
          width: w / 2.4,
          child: Tab(
            text: 'Доставка',
          )),
      Container(width: w / 2.4, child: Tab(text: 'Самовывоз')),
      //  Container(width: w / 2.4, child: Tab(text: 'Предзаказ')),
    ];
    print('order page rebuilt');
    return Scaffold(
      appBar: AppBar(
        leading: LeadingButton(),
        backgroundColor: Colors.white,
        title: Text(
          'Оформление заказа',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
        bottom: TabBar(
            indicatorColor: Colors.black,
            unselectedLabelColor: Colors.black38,
            labelColor: Colors.black,
            isScrollable: true,
            onTap: (index) {
              loginProvide.changeDeliverMap('delivery_type_id', index);
            },
            controller: _controllerTab,
            tabs: tabs),
      ),
      body: TabBarView(controller: _controllerTab, children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: w - 32,
            height: h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8), topRight: Radius.circular(8)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 16,
                  ),

                  SizedBox(
                    height: 24,
                  ),
                  CustomTextField(
                    controller: addressField,
                    onChanged: (val) {
                      loginProvide.changeDeliverMap('address', val);
                    },
                    headText: 'Адресс доставки',
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 0.1),
                    child: Row(
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: w / 2 - 22,
                                //margin: const EdgeInsets.only(right: 10),

                                child: CustomTextField(
                                  controller: appartmentField,
                                  onChanged: (val) {
                                    loginProvide.changeDeliverMap(
                                        'apartment', val);
                                  },
                                  headText: 'Номер квартиры',
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                            // width: 16,
                            ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 8,
                              ),
                              Container(
                                width: w / 2 - 27,
                                child: CustomTextField(
                                  controller: entranceField,
                                  onChanged: (val) {
                                    loginProvide.changeDeliverMap('porch', val);
                                  },
                                  headText: 'Подъезд',
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Комментарий курьеру',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Container(
                            width: w - 64,
                            height: 150,
                            child: TextField(
                              maxLines: 5,
                              cursorColor: Colors.black,
                              decoration: new InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius: new BorderRadius.circular(8.0),
                                ),
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  borderSide:
                                      new BorderSide(color: Colors.black),
                                ),
                              ),
                              keyboardType: TextInputType.multiline,
                              style: new TextStyle(
                                color: Colors.black,
                              ),
                              onChanged: (val) {
                                loginProvide.changeDeliverMap('comments', val);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 32),
                    child: Text(
                      'Уточнить дату и время',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  ChooseOption(
                    text: 'Как можно скорееe',
                    // isWithMe: widget.provide.isWithMe,
                    value: asap,
                    onTap: () {
                      setState(() {
                        asap = !asap;
                      });
                      loginProvide.changeDeliverMap('asap', asap);
                    },
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: [
                      ChooseOption(
                        text: 'Доставить ко времени',
                        //  isWithMe: widget.provide.isWithMe,
                        value: !asap,
                        onTap: () {
                          setState(() {
                            asap = !asap;
                          });
                        },
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Container(
                        child: Container(
                          width: 72,
                          height: 32,
                          child: TextFormField(
                            decoration: new InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                                borderRadius: new BorderRadius.circular(8.0),
                              ),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(8.0),
                                borderSide: new BorderSide(color: Colors.black),
                              ),
                            ),
                            controller: timeCtl, // add this line.
                            onTap: () async {
                              TimeOfDay time = TimeOfDay.now();
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              TimeOfDay picked = await showTimePicker(
                                context: context,
                                initialTime: time,
                                helpText: 'Выберите время',
                                cancelText: 'Отменить',
                                confirmText: 'Готово',
                              );
                              if (picked != null && picked != time) {
                                timeCtl.text =
                                    picked.toString(); // add this line.
                                setState(() {
                                  time = picked;
                                  timeCtl.text = picked.format(context);
                                });
                                //  widget.provide.changeIsWithMe(1);
                                // loginProvide.changeDeliverMap(
                                //     'delivery_time', timeCtl.text);
                              }
                            },
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Зполните время';
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 32),
                    child: Text(
                      'Способ оплаты',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: [
                      ChooseOption(
                        text: 'Оплатить онлайн',
                        //isWithMe: widget.provide.cashType,
                        value: !isCash,
                        onTap: () {
                          loginProvide.changeDeliverMap('payment_type_id', 0);
                          setState(() {
                            isCash = false;
                          });
                        },
                      ),
                      ChooseOption(
                        text: 'Наличными',
                        // isWithMe: widget.provide.cashType,
                        value: isCash,
                        onTap: () {
                          loginProvide.changeDeliverMap('payment_type_id', 1);
                          setState(() {
                            isCash = true;
                          });
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  //----------------------------------------------------------------------------------------------
                  SizedBox(
                    height: 24,
                  ),
                  OrderEndPoint()
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            width: w - 32,
            height: h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8), topRight: Radius.circular(8)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 32),
                    child: Text(
                      'Наши точки доставки',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  ChooseOption(
                    text: 'Ул. Мухита 78 Б',
                    // isWithMe: provide.isWithMe,
                    value: address,

                    onTap: () {
                      setState(() {
                        address = true;
                      });
                      // provide.changeIsWithMe(0);
                      // loginProvide.changeNotDeliverMap(
                      //     'address', 'Ул. Мухита 78 Б');
                    },
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  ChooseOption(
                    text: 'мкр. Строитель 26/3',
                    // isWithMe: provide.isWithMe,
                    value: !address,
                    onTap: () {
                      setState(() {
                        address = false;
                      });
                      // provide.changeIsWithMe(1);
                      // loginProvide.changeNotDeliverMap(
                      //     'address', 'мкр. Строитель 26/3');
                    },
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 32),
                    child: Text(
                      'Уточнить дату и время',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  ChooseOption(
                    text: 'Как можно скорееe',
                    // isWithMe: widget.provide.isWithMe,
                    value: asap,
                    onTap: () {
                      setState(() {
                        asap = !asap;
                      });
                      loginProvide.changeDeliverMap('asap', asap);
                    },
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: [
                      ChooseOption(
                        text: 'Забрать в',
                        // isWithMe: provide.shipPoints,
                        value: !asap,
                        onTap: () {
                          setState(() {
                            asap = !asap;
                          });
                        },
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Container(
                        child: Container(
                          width: 72,
                          height: 32,
                          child: TextFormField(
                            decoration: new InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                                borderRadius: new BorderRadius.circular(8.0),
                              ),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(8.0),
                                borderSide: new BorderSide(color: Colors.black),
                              ),
                            ),
                            controller: timeCtl, // add this line.
                            onTap: () async {
                              TimeOfDay time = TimeOfDay.now();
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              TimeOfDay picked = await showTimePicker(
                                context: context,
                                initialTime: time,
                                helpText: 'Выберите время',
                                cancelText: 'Отменить',
                                confirmText: 'Готово',
                              );
                              if (picked != null && picked != time) {
                                setState(() {
                                  time = picked;
                                  timeCtl.text = picked.format(context);
                                });
                                // provide.changeShipPoint(1);
                                // loginProvide.changeNotDeliverMap(
                                //     'delivery_time', timeCtl.text);
                              }
                            },
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Заполните время';
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 32, top: 32),
                    child: Text(
                      'Способ оплаты',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 24.0),
                    child: Row(
                      children: [
                        ChooseOption(
                          text: 'Оплатить онлайн',
                          //isWithMe: widget.provide.cashType,
                          value: !isCash,
                          onTap: () {
                            loginProvide.changeDeliverMap('payment_type_id', 0);
                            setState(() {
                              isCash = false;
                            });
                          },
                        ),
                        ChooseOption(
                          text: 'Наличными',
                          // isWithMe: widget.provide.cashType,
                          value: isCash,
                          onTap: () {
                            loginProvide.changeDeliverMap('payment_type_id', 1);
                            setState(() {
                              isCash = true;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  // CustomTextField(
                  //   headText: 'Нужна сдача с',
                  //   onChanged: (val) {
                  //     // loginProvide.changeNotDeliverMap(
                  //     //     'surrender', val);
                  //   },
                  // ),
                  SizedBox(
                    height: 16,
                  ),
                  OrderEndPoint(
                    delivery: false,
                  )
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }
}

class ChooseOption extends StatelessWidget {
  final text;
  final onTap;
  final bool value;
  ChooseOption(
      {@required this.text, @required this.onTap, @required this.value});
  @override
  Widget build(BuildContext context) {
    var secondaryTextStyle = TextStyle(
      color: Color(0xff0D0D0D),
      fontWeight: FontWeight.normal,
      fontSize: 12,
    );
    return Padding(
      padding: const EdgeInsets.only(left: 32),
      child: GestureDetector(
        onTap: onTap,
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.all(2),
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(
                      width: 2, color: Theme.of(context).primaryColor)),
              child: value
                  ? Container(
                      width: 5,
                      height: 5,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(100),
                      ),
                    )
                  : SizedBox(),
            ),
            SizedBox(
              width: 16,
            ),
            Text(
              text,
              style: secondaryTextStyle,
            ),
          ],
        ),
      ),
    );
  }
}

class OrderEndPoint extends StatelessWidget {
  final bool delivery;

  const OrderEndPoint({Key key, this.delivery = true}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var cartProvider = Provider.of<ProductsControllerUI>(context);
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 32),
          decoration: BoxDecoration(
              border:
                  Border.all(width: 1, color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 32, top: 16),
                child: Text(
                  'Итого:',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
              ),
              if (delivery)
                Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 32, top: 16, right: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Доставка:',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            cartProvider.deliveryPrice.toString() + ' тг',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              Padding(
                padding: const EdgeInsets.only(left: 32, top: 16, right: 32),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Сумма заказа:',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      cartProvider.sum.toString() + ' тг',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 32, top: 16, right: 32, bottom: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Итого к оплате:',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 12,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      (cartProvider.sum +
                                  (delivery ? cartProvider.deliveryPrice : 0))
                              .toString() +
                          ' тг',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 12,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            var userToken =
                RepositoryProvider.of<AuthenticationRepository>(context)
                    .getToken();
            if (userToken == null) {
              cartProvider.showAlert(context, 'Вы не авторизованы',
                  'Пожалуйста авторизуйтесь!', true);
              return 'error';
            }
            var basket = jsonEncode(cartProvider.cartItems);
            cartProvider.order(basket, context, userToken);
          },
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32),
            height: 50,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10))),
            child: Center(
              child: Text(
                'Заказать за ' +
                    (cartProvider.sum +
                            (delivery ? cartProvider.deliveryPrice : 0))
                        .toString(),
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
