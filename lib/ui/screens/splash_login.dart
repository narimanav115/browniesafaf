import 'package:brownies/data/blocs/login/bloc/auth_bloc.dart';
import 'package:brownies/data/blocs/products/bloc.dart';
import 'package:brownies/data/blocs/products/events.dart';
import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/screens/home_page.dart';
import 'package:brownies/ui/screens/profile/authorization.dart';
import 'package:brownies/ui/screens/profile/registration.dart';
import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class SplashScreenLogin extends StatelessWidget {
  static var id = 'authorization';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (context, state) {
        print(state.status);
        switch (state.status) {
          case AuthenticationStatus.authenticated:
            Navigator.pushAndRemoveUntil(context,
                MaterialPageRoute(builder: (context) {
              BlocProvider.of<ProductsBloc>(context).add(ProductLoadEvent());
              return HomePage();
            }), (route) => false);
            break;
          case AuthenticationStatus.error:
            Provider.of<ProductsControllerUI>(context, listen: false)
                .showAlert(context, 'Ошибка', state.error, true);
            break;
          default:
            break;
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    flex: 1,
                    child: SizedBox(
                      height: h,
                    ),
                  ),
                  Image.asset('assets/logo.png'),
                  Flexible(
                    flex: 1,
                    child: SizedBox(
                      height: h,
                    ),
                  ),
                  ActionButton(
                    text: 'Войти',
                    enable: true,
                    onTap: () {
                      Navigator.pushNamed(context, AuthorizationScreen.id);
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  RegisterButton(
                    onTap: () {
                      Navigator.pushNamed(context, RegisterScreen.id);
                    },
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  GestureDetector(
                    onTap: () async {
                      BlocProvider.of<ProductsBloc>(context)
                          .add(ProductLoadEvent());
                      Navigator.pushNamed(context, HomePage.id);
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 80),
                      // width: 50,
                      height: 50,
                      child: Center(
                        child: Text(
                          'Войти без авторизации',
                          style: TextStyle(
                            color: purpleBrownies,
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                            //  decoration: TextDecoration.underline
                          ),
                          //enable: true,
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: SizedBox(
                      height: h,
                    ),
                  ),
                  GestureDetector(
                      onTap: () {
                        // if (Provider.of<LoginProvide>(context, listen: false)
                        //     .adressesArray
                        //     .length >
                        //     0) {
                        //   Navigator.pushNamed(context, AboutUs.id);
                        // }
                      },
                      child: Text(
                        'О нас',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline),
                      )),
                  SizedBox(
                    height: 24,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class RegisterButton extends StatelessWidget {
  final onTap;
  RegisterButton({this.onTap});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);

    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: w - 32,
        padding: EdgeInsets.symmetric(vertical: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: theme.primaryColor),
        ),
        child: Center(
          child: Text(
            'Регистрация',
            style: TextStyle(
                color: theme.primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 16),
          ),
        ),
      ),
    );
  }
}
