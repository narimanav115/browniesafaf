import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:brownies/ui/configs_themes/icons.dart';
import 'package:brownies/ui/configs_themes/no_glow_scroll.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:brownies/ui/widgets/buttons/double_coloredButton.dart';
import 'package:brownies/ui/widgets/model_containers/component_container.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductInfo extends StatefulWidget {
  static var id = 'productInfo';
  final Product product;

  ProductInfo(this.product) : super();

  @override
  _ProductInfoState createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
        leading: Container(
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                    width: 40,
                    height: 40,
                    decoration: ShapeDecoration(
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                            width: 1.0, style: BorderStyle.solid, color: grey),
                        borderRadius: BorderRadius.all(Radius.circular(32)),
                      ),
                    )),
                Icon(
                  Brownies.back_arrow,
                  color: purpleBrownies,
                  size: 30,
                ),
              ],
            ),
          ),
        ),
        title: Image.asset(
          'assets/title.png',
          scale: 4,
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: IOSScrollingBehaviourColumn(
        product: widget.product,
      ),
      // bottomNavigationBar: CartButton()
    );
  }
}

class IOSScrollingBehaviourColumn extends StatefulWidget {
  final product;

  const IOSScrollingBehaviourColumn({Key key, this.product}) : super(key: key);
  @override
  _IOSScrollingBehaviourColumnState createState() =>
      _IOSScrollingBehaviourColumnState();
}

class _IOSScrollingBehaviourColumnState
    extends State<IOSScrollingBehaviourColumn> {
  ScrollController controller = ScrollController();
  double size = 1;

  @override
  void initState() {
    controller = ScrollController()
      ..addListener(() {
        size = controller.offset / 3.0;
        setState(() {});
      });
    super.initState();
  }

  // @override
  // void dispose() {
  //   // Provider.of<ProductsControllerUI>(context).resetComponents();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    var cartProvider = Provider.of<ProductsControllerUI>(context);
    var indexInCart = cartProvider.cartItems.indexOf(widget.product);
    double width = MediaQuery.of(context).size.width - 32;
    if (width < size) size = width - 1;
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      controller: controller,
      child: Column(
        children: [
          Container(
            width: width - size,
            // height: size*2,
            //padding: EdgeInsets.only(top: 90),
            child: Center(
                child: new AspectRatio(
              aspectRatio: 1,
              child: CachedNetworkImage(
                imageUrl: widget.product.img,
                fit: BoxFit.cover,
              ),
            )),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 12),
                Text(
                  widget.product.title,
                  style: TextStyle(
                      color: purpleBrownies,
                      fontWeight: FontWeight.w700,
                      fontSize: 24),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  widget.product.description,
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                ),
                SizedBox(
                  height: 16,
                ),
                widget.product.fluid == 1
                    ? VariationTab(
                        product: widget.product,
                      )
                    : Container(),
                SizedBox(
                  height: 16,
                ),
                DoubleColoredButton(
                  firstText: 'Добавить',
                  secondText: widget.product.fluid == 1
                      ? widget.product.variation.price
                      : (cartProvider.getPriceOfProduct(widget.product))
                              .toString() +
                          ' ₸',
                  onTap: () async {
                    cartProvider.addToCart(widget.product);
                    Navigator.pop(context);
                    // Scaffold.of(context).context;
                    await showCupertinoDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (context) {
                          Future.delayed(Duration(seconds: 2), () {
                            Navigator.of(context).pop();
                          });
                          return CupertinoAlertDialog(
                            title: Text(
                              'Продукт добавлен! \n ',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            content: Icon(
                              Brownies.shopping_cartC,
                              color: purpleBrownies,
                              size: 150,
                            ),
                          );
                        });
                  },
                ),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.width * 0.9,
            child: ScrollConfiguration(
              behavior: MyBehavior(),
              child: GridView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
                scrollDirection: Axis.horizontal,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisSpacing: 24,
                    mainAxisSpacing: 30,
                    childAspectRatio: 1,
                    crossAxisCount: 2),
                itemBuilder: (context, i) {
                  //final items = product.components[i];
                  // Provider.of<ComponentsProvider>(scaffoldKey.currentContext, listen: false).assignComponents('nothing in here');

                  return ComponentContainer(
                      indexInCart, widget.product, cartProvider.components[i]);
                },
                itemCount: cartProvider.components.length,
                physics: AlwaysScrollableScrollPhysics(),
              ),
            ),
          ),
          SizedBox(
            height: 150,
          )
        ],
      ),
    );
  }
}
