import 'package:brownies/data/blocs/login/bloc/auth_bloc.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/screens/profile/about_us.dart';
import 'package:brownies/ui/screens/profile/bonus_info.dart';
import 'package:brownies/ui/screens/profile/delivery_info.dart';
import 'package:brownies/ui/screens/profile/profile_edit.dart';
import 'package:brownies/ui/screens/profile/settings.dart';
import 'package:brownies/ui/screens/splash_login.dart';
import 'package:brownies/ui/widgets/appbar/buttons/main_appbar.dart';
import 'package:brownies/ui/widgets/styledContainers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfilePage extends StatelessWidget {
  static var id = 'profilePage';
  static var routes = [
    DeliveryInfo.id,
    BonusInfo.id,
    Settings.id,
    AboutUs.id,
    'Выход',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      appBar: CustomAppBar(),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: backgroundDecor,
          child: Column(
            children: [
              BlocBuilder<AuthenticationBloc, AuthenticationState>(
                  buildWhen: (previous, current) =>
                      previous.status != current.status,
                  builder: (context, state) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfileEdit()));
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 87,
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  offset: Offset(4, 4),
                                  blurRadius: 4)
                            ],
                            color: Colors.white,
                          ),
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 42.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 0),
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          state.user.name ?? 'Без имени',
                                          style: textStyleBlue,
                                        ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text(
                                          state.user.phone ?? '',
                                          style: textStyle,
                                        ),
                                      ]),
                                ),
                                Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Container(
                                        width: 40,
                                        height: 40,
                                        decoration: ShapeDecoration(
                                          shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                                width: 1.0,
                                                style: BorderStyle.solid,
                                                color: blueBrownies
                                                    .withOpacity(0.75)),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(32)),
                                          ),
                                        )),
                                    Text(
                                      'RU',
                                      style: textStyleBlue,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          )),
                    );
                  }),
              SizedBox(
                height: 32,
              ),
              BlocBuilder<AuthenticationBloc, AuthenticationState>(
                  buildWhen: (previous, current) =>
                      previous.status != current.status,
                  builder: (context, state) {
                    return ListView.builder(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      shrinkWrap: true,
                      itemBuilder: (context, i) {
                        return GestureDetector(
                          onTap: () {
                            if (routes[i] != 'Выход') {
                              Navigator.pushNamed(context, routes[i]);
                            } else {
                              BlocProvider.of<LoginBloc>(context)
                                  .add(LogoutSubmitted());
                              // context.read<AuthenticationBloc>().add(AuthenticationStatusChanged(AuthenticationStatus.unauthenticated));
                              // print(state.status);

                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          SplashScreenLogin()),
                                  (route) => false);
                            }
                          },
                          child: ContainerShadowed(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 28, vertical: 4),
                              height: 52,
                              children: [
                                Text(
                                  routes[i],
                                  style: textStyle,
                                )
                              ]),
                        );
                      },
                      itemCount: 5,
                      physics: NeverScrollableScrollPhysics(),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}
