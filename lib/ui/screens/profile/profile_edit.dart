import 'package:brownies/data/blocs/edit_profile/edit_profile_bloc.dart';
import 'package:brownies/data/blocs/login/bloc/auth_bloc.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:brownies/ui/widgets/appbar/buttons/main_appbar.dart';
import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:brownies/ui/widgets/text_field_custom.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pattern_formatter/date_formatter.dart';

class ProfileEdit extends StatefulWidget {
  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  TextEditingController date = TextEditingController();
  TextEditingController controller = TextEditingController();

  var mapBody = {};

  String cityName;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      AuthenticationBloc od = (BlocProvider.of<AuthenticationBloc>(context));
      mapBody['name'] = od.state.user.name;
      mapBody['phone'] = od.state.user.phone ?? '';
      // mapBody['city'] = od.state.user.city ?? '';
      // mapBody['address'] = od.state.user.address ?? '';
      // mapBody['porch'] = od.state.user.porch ?? '';
      // mapBody['apartment'] = od.state.user.apartment ?? '';
      print('bday is ${od.state.user.bday}');
      mapBody['bday'] = od.state.user.bday ?? '';
      setState(() {
        cityName =
            (BlocProvider.of<AuthenticationBloc>(context).state.user.city);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    print(BlocProvider.of<AuthenticationBloc>(context).state.user.city);
    var user = BlocProvider.of<AuthenticationBloc>(context).state.user;
    return Scaffold(
      appBar: CustomAppBar(),
      body: BlocProvider(
        create: (context) {
          return EditProfileBloc(
            authenticationRepository:
                RepositoryProvider.of<AuthenticationRepository>(context),
          );
        },
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                hintText: user.name,
                headText: 'Имя',
                onChanged: (e) {
                  mapBody['name'] = e;
                },
              ),
              CustomTextField(
                hintText: user.phone,
                headText: 'Номер телефона',
                keyBoard: TextInputType.phone,
                onChanged: (e) {
                  mapBody['phone'] = e;
                },
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Дата рождения',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width - 64,
                        height: 50,
                        child: TextField(
                          cursorColor: Colors.black,
                          inputFormatters: [
                            DateInputFormatter(),
                          ],
                          decoration: new InputDecoration(
                            hintText: mapBody['bday'],
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                              borderRadius: new BorderRadius.circular(8.0),
                            ),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(8.0),
                              borderSide: new BorderSide(color: Colors.black),
                            ),
                          ),
                          keyboardType: TextInputType.datetime,
                          style: new TextStyle(
                            color: Colors.black,
                          ),
                          onChanged: (e) {
                            print(e);
                            mapBody['bday'] = e;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              BlocBuilder<EditProfileBloc, EditProfileState>(
                  builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32.0),
                  child: ActionButton(
                    text: 'Сохранить',
                    onTap: () async {
                      print(BlocProvider.of<AuthenticationBloc>(context)
                          .state
                          .user
                          .name);
                      print(BlocProvider.of<AuthenticationBloc>(context)
                          .state
                          .user
                          .city);
                      context
                          .read<EditProfileBloc>()
                          .add(EditSubmitDone(mapBody));
                      Navigator.pop(context);
                    },
                    enable: true,
                  ),
                );
              }),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
