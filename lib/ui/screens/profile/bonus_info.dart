import 'package:brownies/data/blocs/login/bloc/auth_bloc.dart';
import 'package:brownies/ui/configs_themes/icons.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/widgets/appbar/buttons/main_appbar.dart';
import 'package:brownies/ui/widgets/styledContainers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BonusInfo extends StatelessWidget {
  static var id = 'Бонусы';
  static var style = TextStyle(fontWeight: FontWeight.w500, fontSize: 15);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(),
        body: Container(
          decoration: backgroundDecor,
          child: SingleChildScrollView(
            child: Column(
              children: [
                ContainerShadowed(
                  height: 50,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Накоплено бонусов',
                            style: style,
                          ),
                          Row(
                            children: [
                              Text(
                                BlocProvider.of<AuthenticationBloc>(context)
                                        .state
                                        .user
                                        .bonus
                                        .toString() +
                                    ' ',
                                style: TextStyle(color: purpleBrownies),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Icon(Brownies.pie, color: purpleBrownies)
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                ContainerShadowed(
                  mainAxisAlignment: MainAxisAlignment.start,
                  height: 275,
                  children: [
                    Center(
                      child: Container(
                          padding: EdgeInsets.only(top: 16),
                          width: 136,
                          height: 170,
                          child: Stack(children: <Widget>[
                            Positioned(
                                top: 0,
                                left: 0,
                                child: Container(
                                    width: 136,
                                    height: 136,
                                    decoration: BoxDecoration(
                                      color: Color.fromRGBO(254, 0, 174, 1),
                                      borderRadius: BorderRadius.all(
                                          Radius.elliptical(136, 136)),
                                    ))),
                            Positioned(
                                top: 10,
                                left: 11,
                                child: Container(
                                    width: 115,
                                    height: 115,
                                    decoration: BoxDecoration(
                                      color: Color.fromRGBO(254, 0, 174, 1),
                                      border: Border.all(
                                        color: Color.fromRGBO(255, 255, 255, 1),
                                        width: 6,
                                      ),
                                      borderRadius: BorderRadius.all(
                                          Radius.elliptical(115, 115)),
                                    ))),
                            Positioned(
                                top: 38,
                                left: 38,
                                child: Icon(
                                  Brownies.double_card,
                                  size: 60,
                                  color: Colors.white,
                                )),
                          ])),
                    ),
                    Text(
                      '        Программа бонусов «БРАУНИС» ',
                      style: textStyleBlue,
                    ),
                    Text(
                        'Получайте вознаграждение за каждую покупку в ресторанах «Браунис». '
                        'Копите бонусы и делайте любые приобретения в “Браунис”'),
                  ],
                ),
                ContainerShadowed(
                  mainAxisAlignment: MainAxisAlignment.start,
                  height: 275,
                  children: [
                    Center(
                      child: Container(
                          padding: EdgeInsets.only(top: 16),
                          width: 136,
                          height: 170,
                          child: Image.asset('assets/discount.png')

                          // Stack(children: <Widget>[
                          //   Positioned(
                          //       top: 0,
                          //       left: 0,
                          //       child: Container(
                          //           width: 136,
                          //           height: 136,
                          //           decoration: BoxDecoration(
                          //             color: Color.fromRGBO(254, 0, 174, 1),
                          //             borderRadius: BorderRadius.all(
                          //                 Radius.elliptical(136, 136)),
                          //           ))),
                          //   Positioned(
                          //       top: 10,
                          //       left: 11,
                          //       child: Container(
                          //           width: 115,
                          //           height: 115,
                          //           decoration: BoxDecoration(
                          //             color: Color.fromRGBO(254, 0, 174, 1),
                          //             border: Border.all(
                          //               color: Color.fromRGBO(255, 255, 255, 1),
                          //               width: 6,
                          //             ),
                          //             borderRadius: BorderRadius.all(
                          //                 Radius.elliptical(115, 115)),
                          //           ))),
                          //   Positioned(
                          //       top: 34,
                          //       left: 35,
                          //       child: RichText(
                          //         textAlign: TextAlign.center,
                          //         text: TextSpan(
                          //             text: 'до',
                          //             style: TextStyle(
                          //                 color: Colors.white,
                          //                 fontWeight: FontWeight.w700,
                          //                 fontSize: 18),
                          //             children: [
                          //               TextSpan(
                          //                 text: "\n90%",
                          //                 style: TextStyle(
                          //                     color: Colors.white,
                          //                     fontWeight: FontWeight.w700,
                          //                     fontSize: 35),
                          //               )
                          //             ]),
                          //       )),
                          // ])

                          ),
                    ),
                    Text(
                      'Бонусами можно оплатить до 90 ,99% любой покупки!',
                      style: textStyleBlue,
                    ),
                    Text('Каждая новая покупка оплачивает следующую!'),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
