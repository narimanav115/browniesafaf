import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:brownies/ui/widgets/text_field_custom.dart';
import 'package:flutter/material.dart';

class NewPassword extends StatelessWidget {
  static var id = 'newPassword';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    // var loginProvide = Provider.of<CitiesProvider>(context);
    // var canTap = loginProvide.newPassword['code']!= null && loginProvide.newPassword['password']!= null &&  loginProvide.newPassword['password']!= '' && loginProvide.newPassword['password_confirmation']!= null && loginProvide.newPassword['password_confirmation']!= '';
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Text(
                'Сброс пароля',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Flexible(
              flex: 1,
              child: SizedBox(
                height: h,
              ),
            ),
            SizedBox(
              height: 28,
            ),
            CustomTextField(
              headText: 'Код',
              onChanged: (val) {},
            ),
            SizedBox(
              height: 28,
            ),
            CustomTextField(
              headText: 'Новый пароль',
              obscure: true,
              onChanged: (val) {},
            ),
            SizedBox(
              height: 28,
            ),
            CustomTextField(
              headText: 'Введите пароль повторно',
              obscure: true,
              onChanged: (val) {},
            ),
            SizedBox(
              height: 28,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: ActionButton(
                text: 'Отправить письмо',
                // enable:canTap,
                onTap: () {},
                // trigger: loginProvide.forgotLoading,
              ),
            ),
            Flexible(
              flex: 3,
              child: SizedBox(
                height: h,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
