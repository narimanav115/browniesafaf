import 'package:brownies/data/blocs/login/bloc/auth_bloc.dart';
import 'package:brownies/ui/screens/profile/registration.dart';
import 'package:brownies/ui/screens/profile/restoration.dart';
import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:brownies/ui/widgets/text_field_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';

class AuthorizationScreen extends StatefulWidget {
  static var id = '2auth';

  @override
  _AuthorizationScreenState createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {
  String phoneNumber = '';

  var phoneFormatter = new MaskTextInputFormatter(
      mask: '+7 (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Text(
                  'Авторизация',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: h * 0.1,
              ),
              BlocBuilder<LoginBloc, LoginState>(
                  buildWhen: (previous, current) =>
                      previous.username != current.username,
                  builder: (context, state) {
                    return CustomTextField(
                      headText: 'Телефон',
                      key: const Key('loginForm_usernameInput_textField'),
                      inputFormatters: [phoneFormatter],
                      onChanged: (phoneValue) {
                        phoneNumber =
                            phoneValue.replaceAll(new RegExp(r"\D"), "");
                        context
                            .read<LoginBloc>()
                            .add(LoginUsernameChanged(phoneNumber));
                      },
                    );
                  }),
              SizedBox(
                height: 28,
              ),
              BlocBuilder<LoginBloc, LoginState>(
                  buildWhen: (previous, current) =>
                      previous.password != current.password,
                  builder: (context, state) {
                    return CustomTextField(
                      headText: 'Пароль',
                      key: const Key('loginForm_passwordInput_textField'),
                      onChanged: (password) => context
                          .read<LoginBloc>()
                          .add(LoginPasswordChanged(password)),
                      obscure: true,
                    );
                  }),
              Padding(
                padding: const EdgeInsets.only(
                    left: 32, right: 32, top: 8, bottom: 32),
                child: Container(
                  width: w - 64,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, ForgotPassword.id);
                    },
                    child: Text(
                      'Забыл пароль',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                      ),
                      textAlign: TextAlign.end,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 32),
                child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, RegisterScreen.id);
                    },
                    child: Text(
                      'Регистрация',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          decoration: TextDecoration.underline),
                    )),
              ),
              BlocBuilder<LoginBloc, LoginState>(
                  buildWhen: (previous, current) =>
                      previous.status != current.status,
                  builder: (context, state) {
                    return state.status.isSubmissionInProgress
                        ? const CircularProgressIndicator()
                        : Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 30.0),
                            child: ActionButton(
                              text: 'Войти',
                              enable: true,
                              onTap: state.status.isValidated
                                  ? () async {
                                      print(BlocProvider.of<LoginBloc>(context)
                                          .state
                                          .status);
                                      context
                                          .read<LoginBloc>()
                                          .add(const LoginSubmitted());
                                    }
                                  : null,
                              // trigger: loginProvide.isLoading,
                            ),
                          );
                  }),
              Flexible(
                flex: 3,
                child: SizedBox(
                  height: h,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
