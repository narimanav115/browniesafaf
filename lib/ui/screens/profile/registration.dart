import 'package:brownies/data/blocs/registration/registration_bloc.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:brownies/ui/widgets/text_field_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  static var id = 'register';

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String email = '';
  String phone = '';
  String password = '';

  var phoneFormatter = new MaskTextInputFormatter(
      mask: '+7 (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: BlocProvider(
        create: (context) {
          return RegistrationBloc(
            authenticationRepository:
                RepositoryProvider.of<AuthenticationRepository>(context),
          );
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.only(top: 24),
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                      child: Text(
                    'Регистрация',
                    style: TextStyle(
                        color: purpleBrownies,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  )),
                ),
                SizedBox(
                  height: h * 0.1 / 1.5,
                ),
                BlocBuilder<RegistrationBloc, RegistrationState>(
                    buildWhen: (previous, current) =>
                        previous.username != current.username,
                    builder: (context, state) {
                      return CustomTextField(
                        headText: 'Телефон',
                        key: const Key('reg_loginForm_usernameInput_textField'),
                        inputFormatters: [phoneFormatter],
                        keyBoard: TextInputType.phone,
                        onChanged: (phoneValue) {
                          phone = phoneValue.replaceAll(new RegExp(r"\D"), "");
                          context
                              .read<RegistrationBloc>()
                              .add(RegLoginUsernameChanged(phone));
                        },
                      );
                    }),
                SizedBox(
                  height: 28,
                ),
                BlocBuilder<RegistrationBloc, RegistrationState>(
                    buildWhen: (previous, current) =>
                        previous.password != current.password,
                    builder: (context, state) {
                      return CustomTextField(
                        headText: 'Пароль',
                        key: const Key('reg_loginForm_passwordInput_textField'),
                        onChanged: (passwordV) => context
                            .read<RegistrationBloc>()
                            .add(RegLoginPasswordChanged(passwordV)),
                        obscure: true,
                      );
                    }),
                SizedBox(
                  height: 28,
                ),
                CustomTextField(
                  onChanged: (e) {
                    email = e;
                  },
                  headText: 'E-mail',
                  key: const Key('reg_loginForm_emailInput_textField'),
                  // obscure: true,
                ),
                BlocBuilder<RegistrationBloc, RegistrationState>(
                    buildWhen: (previous, current) =>
                        previous.status != current.status,
                    builder: (context, state) {
                      return state.status.isSubmissionInProgress
                          ? const CircularProgressIndicator()
                          : Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 32, vertical: 70),
                              child: ActionButton(
                                enable: true,
                                onTap: state.status.isValidated
                                    ? () {
                                        context.read<RegistrationBloc>().add(
                                            RegLoginSubmitted(email: email));
                                      }
                                    : null,
                                text: 'Регистрация',
                              ),
                            );
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
