import 'dart:async';
import 'dart:convert';

import 'package:brownies/data/providers/cities_provider.dart';
import 'package:brownies/repository/models/cities_model.dart';
import 'package:brownies/ui/configs_themes/icons.dart';
import 'package:brownies/ui/configs_themes/no_glow_scroll.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/widgets/appbar/buttons/bar_elements.dart';
import 'package:brownies/ui/widgets/styledContainers.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutUs extends StatefulWidget {
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  static var id = 'Контакты';

  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  final Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;
  double latitude;
  double longitude;

  Future<void> convert(String args) async {
    mapController = await _controller.future;

    print(args);
    final response = await http.get(
      Uri.parse('https://geocode-maps.yandex.ru/1.x/?format=json&apikey=3a0d7e1e-4ee6-4fc9-8cb5-30c312a0250d&geocode=$args'),
    );
    final statusCode = response.statusCode;
    if (statusCode == 200) {
      final data = json.decode(response.body);
      List<String> point = data["response"]["GeoObjectCollection"]
              ["featureMember"][0]['GeoObject']['Point']['pos']
          .split(' ');
      latitude = double.parse(point[0]);
      longitude = double.parse(point[1]);
      print('$latitude  $longitude');
      // setState(() {
      //   cityPos = CameraPosition(target: LatLng(latitude, longitude), zoom: 1);
      // });
    } else
      print("error: $statusCode");
  }

//43.245890, 76.894080
  CameraPosition cityPos = CameraPosition(
      target: LatLng(
        43.245890,
        76.894080,
      ),
      zoom: 14);

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      String city =
          Provider.of<CitiesProvider>(context, listen: false).chosenCity.city;
      convert(city);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    City chosenCity = Provider.of<CitiesProvider>(context).chosenCity;
    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: LeadingButton(),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(0, 80, 0, 0),
        width: MediaQuery.of(context).size.width,
        decoration: backgroundDecor,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Center(
                    child: Image.asset('assets/title.png', scale: 4),
                  ),
                  ContainerShadowed(
                      height: 88.00,
                      width: 312.00,
                      margin: EdgeInsets.symmetric(vertical: 20),
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10, left: 3),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Телефон:',
                                textAlign: TextAlign.center,
                                style: textStyle,
                              ),
                              Text(
                                '8 (777) 732-32-52',
                                textAlign: TextAlign.left,
                                style: textStyleBlue,
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          height: 1,
                          color: Colors.black38,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10, left: 3),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Почта:',
                                textAlign: TextAlign.left,
                                style: textStyle,
                              ),
                              Text(
                                'braunis@gmail.com',
                                textAlign: TextAlign.left,
                                style: textStyleBlue,
                              ),
                            ],
                          ),
                        ),
                      ]),
                  if (chosenCity.address.isNotEmpty)
                    Container(
                      width: 312,
                      //height: 93,
                      decoration: decorationContainer,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                  height: 44,
                                  padding: EdgeInsets.fromLTRB(8, 14, 8, 8),
                                  child: Text('Точки самовызова')),
                              Divider(
                                height: 1,
                                color: Colors.black38,
                              ),
                              ListView.builder(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                shrinkWrap: true,
                                itemBuilder: (context, i) {
                                  return Container(
                                      padding: EdgeInsets.fromLTRB(8, 15, 8, 8),
                                      height: 44,
                                      child: Text(
                                        chosenCity.address[i].address,
                                        style: textStyleBlue,
                                      ));
                                },
                                itemCount: chosenCity.address.length,
                                physics: NeverScrollableScrollPhysics(),
                              )
                            ]),
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Container(
                      height: 250,
                      color: Colors.black12,
                      child: GoogleMap(
                        mapType: MapType.normal,
                        initialCameraPosition: cityPos,
                        onMapCreated: (GoogleMapController mapper) {
                          _controller.complete(mapper);
                          print('on created: $cityPos');
                          mapper.animateCamera(
                              CameraUpdate.newCameraPosition(cityPos));
                        },
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      var url = 'https://www.instagram.com/browniesqazaqstan';

                      if (await canLaunch(url)) {
                        await launch(
                          url,
                          universalLinksOnly: true,
                        );
                      } else {
                        throw 'There was a problem to open the url: $url';
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 10),
                      width: 312,
                      height: 59,
                      decoration: decorPurple,
                      child: Stack(
                        children: [
                          Positioned(
                            top: 10,
                            left: 20,
                            child: Icon(
                              Brownies.inst,
                              size: 40,
                              color: Colors.white,
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              'Подписаться',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                                fontFamily: "Open Sans",
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  ContainerShadowed(
                    height: 300.00,
                    width: MediaQuery.of(context).size.width,
                    mainAxisAlignment: MainAxisAlignment.start,
                    margin: EdgeInsets.symmetric(vertical: 20, horizontal: 18),
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Text('О Нас'),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                          'С 25 сентября 1991 года мы занимаемся любимым делом и дарим людям радость. Уже более 20 лет наши клиенты разделяют с нами самые важные и счастливые моменты своей жизни. Мы радуемся вместе с ними их достижениям и растем вместе с ними. Натуральное и экологически-чистое сырье, современное европейское оборудование и лучшие кондитеры и позволяют нам производить продукцию для клиентов в разных странах.')
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
