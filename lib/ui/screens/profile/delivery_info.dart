import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/widgets/appbar/buttons/main_appbar.dart';
import 'package:brownies/ui/widgets/styledContainers.dart';
import 'package:flutter/material.dart';

class DeliveryInfo extends StatelessWidget {
  static var id = 'Условия доставки';
  static var style = TextStyle(fontWeight: FontWeight.w500, fontSize: 15);
  static var styleMain = TextStyle(fontWeight: FontWeight.w400, fontSize: 15);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(),
        body: Container(
          decoration: backgroundDecor,
          child: SingleChildScrollView(
            child: Column(
              children: [
                ContainerShadowed(
                  mainAxisAlignment: MainAxisAlignment.start,
                  // height: 200,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Wrap(
                        children: [
                          Text(
                            'Условия доставки',
                            softWrap: true,
                            maxLines: 10,
                            style: style,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    Divider(
                      color: Colors.black,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width - 60,
                        child: Wrap(
                          children: [
                            Text(
                              'Стоимость доставки курьером Доставка товаров курьером по городу Алматы, осуществляется в пределах квадрата улиц: проспекта Аль-Фараби/улицы Саина/проспекта Рыскулова/Восточная объездная. Стоимость доставки товара курьером в указанном выше квадрате составляет - 2 000 тенге. При оформлении заказа на сумму превышающую 15 000 тенге, доставка курьером по городу Алматы в пределах квадрата улиц: проспекта Аль-Фараби/улицы Саина/проспекта Рыскулова/Восточная объездная осуществляется бесплатно.',
                              style: styleMain,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
                ContainerShadowed(
                  mainAxisAlignment: MainAxisAlignment.start,
                  // height: 200,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            'Время доставки',
                            style: style,
                          ),
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.black,
                    ),
                    Wrap(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            'Доставка выполняется ежедневно с 10:00 до 17:00 часов, кроме выходных(сб, вс). Товары, заказанные Вами в субботу и воскресенье, доставляются в понедельник. Время осуществления доставки зависит от времени размещения заказа и наличия товара на складе: если заказ подтвержден менеджером Службы доставки до 12:00, товар может быть доставлен на следующий рабочий день между 10:00 и 14:00 или между 14:00 и 17:00; если заказ подтвержден менеджером Службы доставки после 12:00, товар может быть доставлен на следующий рабочий день между 15:00 и 17:00.',
                            style: styleMain,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
