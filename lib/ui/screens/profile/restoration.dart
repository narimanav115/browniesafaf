import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/authentication_repository.dart';
import 'package:brownies/ui/widgets/buttons/action_button.dart';
import 'package:brownies/ui/widgets/text_field_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class ForgotPassword extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  static var id = 'forgotPassword';
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Text(
                'Восстановление пароля',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Flexible(
              flex: 1,
              child: SizedBox(
                height: h,
              ),
            ),
            CustomTextField(
              controller: emailController,
              headText: 'E-mail',
              onChanged: (val) {
                print(emailController.text);
                // loginProvide.forgotLogin = val;
              },
            ),
            SizedBox(
              height: 24,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: ActionButton(
                text: 'Отправить письмо',
                enable: true,
                onTap: () async {
                  if (emailController.text.contains('@')) {
                    await RepositoryProvider.of<AuthenticationRepository>(
                            context,
                            listen: false)
                        .reset(sendTo: emailController.text);
                    Provider.of<ProductsControllerUI>(context, listen: false)
                        .showAlert(
                            context, 'Пароль выслан на Ваш email', '', false);
                    Navigator.pop(context);
                  } else {
                    Provider.of<ProductsControllerUI>(context, listen: false)
                        .showAlert(context, 'Введите корректный email',
                            'Повторите попытку', true);
                  }
                  // if(canTap){
                  //   loginProvide.sendEmail(context);
                  // }
                },
                //trigger: loginProvide.forgotLoading,),
              ),
            ),
            Flexible(
              flex: 3,
              child: SizedBox(
                height: h,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
