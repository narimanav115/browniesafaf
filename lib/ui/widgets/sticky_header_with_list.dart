import 'package:brownies/configs/url.dart';
import 'package:brownies/data/blocs/products/bloc.dart';
import 'package:brownies/data/blocs/products/states.dart';
import 'package:brownies/data/providers/cities_provider.dart';
import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/models/cities_model.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/widgets/model_containers/container_product.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:provider/provider.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HomePageBody extends StatefulWidget {
  final ProductsLoadedState state;

  const HomePageBody({Key key, this.state}) : super(key: key);
  @override
  _HomePageBodyState createState() => _HomePageBodyState(state);
}

class _HomePageBodyState extends State<HomePageBody>
    with TickerProviderStateMixin {
  final ProductsLoadedState state;
  ScrollController scrollController = ScrollController();
  ScrollController horizontalScroll = ScrollController();
  _HomePageBodyState(this.state);
  TabController tabController;

  @override
  void initState() {
    super.initState();
    var cartProvider =
        Provider.of<ProductsControllerUI>(context, listen: false);
    horizontalScroll = ScrollController();
    tabController = TabController(
        initialIndex: 0, length: state.loadedCategories.length, vsync: this);
    scrollController.addListener(() async {
      await cartProvider.binarySearch(
          state.loadedCategories,
          scrollController.offset,
          0,
          state.loadedCategories.length - 1,
          state.loadedCategories.length - 2);
      tabController.index = cartProvider.tabIndex;
      horizontalScroll.jumpTo(
          scrollController.offset / state.loadedCategories.last.offset * 10);
    });
  }

  @override
  Widget build(BuildContext context) {
    var cityProvider = Provider.of<CitiesProvider>(context);
    // var cartProvider = Provider.of<ProductsControllerUI>(context, listen: false);
    // print(state.banners);
    return DefaultTabController(
      length: state.loadedCategories.length,
      child: SingleChildScrollView(
        //controller: scrollController,
        controller: scrollController,
        child: Column(
          children: [
            Column(
              children: [
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                          width: 1.0, style: BorderStyle.solid, color: grey),
                      borderRadius: BorderRadius.all(Radius.circular(32)),
                    ),
                  ),
                  //height: 100,
                  margin: EdgeInsets.fromLTRB(33, 30, 33, 8),
                  child: DropdownButton<String>(
                    isExpanded: true,
                    underline: Container(),
                    hint: Text('Ваш город'),
                    value: cityProvider.chosenCity.city,
                    icon: Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.keyboard_arrow_down,
                        color: grey,
                      ),
                    ),
                    iconSize: 24,
                    elevation: 0,
                    style: TextStyle(color: Colors.black),
                    onChanged: (String newValue) {
                      // await cartProvider.clearCart();
                      cityProvider.changeCity(newValue, context,
                          BlocProvider.of<ProductsBloc>(context));
                    },
                    items: cityProvider.cities
                        .map<DropdownMenuItem<String>>((City value) {
                      return DropdownMenuItem<String>(
                        value: value.city,
                        child: Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(value.city)),
                      );
                    }).toList(),
                  ),
                ),
                CarouselSlider.builder(
                  itemCount: state.banners?.length,
                  options: CarouselOptions(
                    autoPlay: true,
                    aspectRatio: 1.8,
                  ),
                  itemBuilder: (context, index) {
                    return InkWell(
                      child:
                          Image.network(imgDomain + state.banners[index].image),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Scaffold(
                                      body: SafeArea(
                                        child: WebView(
                                          initialUrl:
                                              state.banners[index].title,
                                          javascriptMode:
                                              JavascriptMode.unrestricted,
                                        ),
                                      ),
                                    )));
                      },
                    );
                  },
                ),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              // direction: Axis.vertical,
              children: [
                StickyHeader(
                  header: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    controller: horizontalScroll,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16),
                      child: Material(
                        color: Colors.transparent,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(5, 5, 5, 8),
                              height: 40,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32)),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 4,
                                      offset: Offset(
                                          0, 1), // changes position of shadow
                                    ),
                                  ],
                                  color: Color(0xffE9E9E9)),
                              child: TabBar(
                                controller: tabController,
                                isScrollable: true,
                                unselectedLabelColor: Colors.black,
                                labelColor: purpleBrownies,
                                indicatorSize: TabBarIndicatorSize.tab,
                                indicator: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 2,
                                        blurRadius: 4,
                                        offset: Offset(
                                            0, 1), // changes position of shadow
                                      ),
                                    ],
                                    color: Colors.white),
                                onTap: (index) async {
                                  // ignore: close_sinks
                                  var bloc =
                                      BlocProvider.of<ProductsBloc>(context);
                                  var pos = bloc.categories[index].offset;
                                  await scrollController.animateTo(pos,
                                      duration:
                                          const Duration(milliseconds: 200),
                                      curve: Curves.easeOut);
                                  tabController.index = index;
                                },
                                tabs: state.loadedCategories
                                    .map((item) => new Text(item.title))
                                    .toList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  content: GroupedListView<Category, Category>(
                    elements: state.loadedCategories,
                    groupBy: (element) => element,
                    shrinkWrap: true,
                    groupSeparatorBuilder: (Category category) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          category.title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 17,
                          ),
                        ),
                      );
                    },
                    indexedItemBuilder: (context, Category element, index) =>
                        Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 8, left: 8),
                          child: GridView.builder(
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisSpacing: 8,
                                    mainAxisSpacing: 24,
                                    childAspectRatio: 0.75,
                                    crossAxisCount: 2),
                            // ignore: missing_return
                            itemBuilder: (context, i) {
                              final Product product = element.productList[i];
                              return ProductContainer(product);
                            },
                            itemCount: element.productList.length,
                            physics: NeverScrollableScrollPhysics(),
                          ),
                        )
                      ],
                    ),
                    itemComparator: (item1, item2) =>
                        ''.compareTo(''), // optio// nal
                    useStickyGroupSeparators: true, //
                    floatingHeader: true, // optional
                    //order: GroupedListOrder.ASC, // optional
                  ),
                ),
                SizedBox(
                  height: 40,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
