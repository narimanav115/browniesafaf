import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatelessWidget {
  final hintText;
  final prefixIcon;
  final onChanged;
  final obscure;
  final TextEditingController controller;
  final headText;
  final List<TextInputFormatter> inputFormatters;
  final TextInputType keyBoard;

  CustomTextField(
      {Key key,
      this.hintText,
      this.prefixIcon,
      this.obscure,
      this.onChanged,
      this.controller,
      this.headText,
      this.keyBoard,
      this.inputFormatters});
  @override
  Widget build(BuildContext context) {
    // var c = Theme.of(context);
    var w = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              headText,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              width: w - 64,
              height: 50,
              child: TextField(
                controller: controller,
                cursorColor: Colors.black,
                obscureText: obscure ?? false,
                inputFormatters: inputFormatters,
                decoration: new InputDecoration(
                  hintText: hintText ?? '',
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                    borderSide: new BorderSide(color: Colors.black),
                  ),
                ),
                keyboardType: this.keyBoard != null
                    ? this.keyBoard
                    : TextInputType.emailAddress,
                style: new TextStyle(
                  color: Colors.black,
                ),
                onChanged: onChanged,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
