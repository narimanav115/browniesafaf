import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/models/cart_product.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ComponentContainer extends StatelessWidget {
  final indexInCart;
  final Product product;
  final Components component;
  static var existStyle = TextStyle(color: Color(0xff3169EE));
  static var notExistStyle = TextStyle(color: Color(0xffFE9800));

  const ComponentContainer(this.indexInCart, this.product, this.component)
      : super();

  @override
  Widget build(BuildContext context) {
    //var provider = Provider.of<ComponentsProvider>(context);
    // ignore: close_sinks
    // ProductsLoadedState state;

    var provider = Provider.of<ProductsControllerUI>(context);
    return GestureDetector(
      onTap: () {
        // provider.changeValue(value, index, productIndex)
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(16)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 4,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ],
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Column(
                children: [
                  SizedBox(
                    height: 4,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: Center(
                        child: new AspectRatio(
                      aspectRatio: 1,
                      child: CachedNetworkImage(
                        imageUrl: component.image,
                        // fit: BoxFit.cover,
                      ),
                    )),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Text(component.componentName),
                  // SizedBox(height: 8),
                ],
              ),
            ),
            Positioned(
              left: 8,
              top: 8,
              child: Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    color: Colors.white,
                  ),
                  child: CircularCheckBox(
                    value: provider.testComp.contains(component),
                    onChanged: (value) {
                      // value
                      //     ? product.price += component.price
                      //     : product.price -= component.price;

                      provider.changeComponents(value, component);
                    },
                    checkColor: Colors.black,
                    activeColor: Colors.white,
                    hoverColor: Colors.green,
                    focusColor: Colors.purple,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
