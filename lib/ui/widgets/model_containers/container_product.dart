import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:brownies/ui/screens/product_info_screen.dart';
import 'package:brownies/ui/widgets/buttons/styled_button.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductContainer extends StatelessWidget {
  final Product product;
  static var existStyle = TextStyle(color: Color(0xff3169EE));
  static var notExistStyle = TextStyle(color: Color(0xffFE9800));

  const ProductContainer(
    this.product,
  ) : super();

  @override
  Widget build(BuildContext context) {
    var cartProvider = Provider.of<ProductsControllerUI>(context);
    return GestureDetector(
      onTap: () async {
        await cartProvider.fetchComponents(product.id, context);
        product.variations = cartProvider.variations;
        if (product.variations.isNotEmpty)
          product.variation = product.variations.first;

        Navigator.push(context, MaterialPageRoute(builder: (_) {
          return ProductInfo(product);
        }));
      },
      child: Container(
        child: Stack(
          children: [
            Positioned(
              left: 5,
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.45,
                      child: Center(
                          child: new AspectRatio(
                        aspectRatio: 1,
                        child: CachedNetworkImage(
                          imageUrl: product.img,
                          fit: BoxFit.cover,
                        ),
                      )),
                    ),
                  ),
                  SizedBox(height: 4),
                  Text(product.title),
                  SizedBox(height: 4),
                  StyledButton(
                    label: product.price.toString(),
                    onTap: () {
                      cartProvider.addToCart(product);
                    },
                  ),
                ],
              ),
            ),
            Positioned(
              top: 2,
              left: 8,
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(4),
                child: product.inStock == 1
                    ? Text('В наличии', style: existStyle)
                    : Text('Предзаказ', style: notExistStyle),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
