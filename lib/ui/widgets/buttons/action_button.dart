import 'package:brownies/data/providers/products_provider.dart';
import 'package:brownies/repository/models/products_model.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ActionButton extends StatelessWidget {
  final text;
  final onTap;
  final enable;
  final trigger;
  final double width;
  ActionButton(
      {this.text,
      this.onTap,
      this.enable = false,
      this.trigger,
      this.width = 400});
  @override
  Widget build(BuildContext context) {
    var c = Theme.of(context);
    return Material(
      color: !enable ? c.buttonColor.withOpacity(0.5) : c.primaryColor,
      borderRadius: BorderRadius.circular(8),
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(16),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 16),
          width: width,
          child: Text(
            text ?? 'null',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: !enable
                    ? Colors.black.withOpacity(0.7)
                    : c.scaffoldBackgroundColor,
                fontSize: 16,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}

class VariationTab extends StatefulWidget {
  final Product product;

  const VariationTab({Key key, this.product}) : super(key: key);

  @override
  _VariationTabState createState() => _VariationTabState();
}

class _VariationTabState extends State<VariationTab> {
  TabController tabController;

  @override
  Widget build(BuildContext context) {
    var cartProvider = Provider.of<ProductsControllerUI>(context);
    // var user = Provider.of<CitiesProvider>(context);
    return DefaultTabController(
      length: cartProvider.variations.length,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 80),
        height: 40,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 4,
                offset: Offset(0, 1), // changes position of shadow
              ),
            ],
            color: Colors.white),
        child: TabBar(
          controller: tabController,
          unselectedLabelColor: Colors.black,
          labelColor: Colors.white,
          indicatorSize: TabBarIndicatorSize.tab,
          indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 4,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
              color: purpleBrownies),
          onTap: (index) async {
            widget.product.variation = cartProvider.variations[index];
            cartProvider.updater();
          },
          tabs: cartProvider.variations
              .map((item) => new Text(item.name))
              .toList(),
        ),
      ),
    );
  }
}
