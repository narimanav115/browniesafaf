import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:flutter/material.dart';

class StyledButton extends StatelessWidget {
  final Function onTap;
  final label;
  final double width;
  final double height;

  const StyledButton(
      {Key key, this.onTap, this.label, this.width = 82.0, this.height = 35.0})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.fromLTRB(2, 2, 0, 0),
        width: width,
        height: height,
        decoration: ShapeDecoration(
          color: Color(0xff0056FE).withOpacity(0.78),
          shape: RoundedRectangleBorder(
            side: BorderSide(width: 1.0, style: BorderStyle.solid, color: grey),
            borderRadius: BorderRadius.all(Radius.circular(16)),
          ),
        ),
        child: Center(
            child: Text(
          label + ' ₸',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 14),
        )),
      ),
    );
  }
}
