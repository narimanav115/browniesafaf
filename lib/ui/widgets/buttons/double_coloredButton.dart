import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:flutter/material.dart';

class DoubleColoredButton extends StatelessWidget {
  final onTap;
  final firstText;
  final secondText;
  final enable;
  final trigger;
  final double width;
  final double height;
  const DoubleColoredButton(
      {Key key,
      this.onTap,
      this.firstText,
      this.enable,
      this.trigger,
      this.width,
      this.height,
      this.secondText})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return Container(
      height: 40,
      child: Center(
        child: InkWell(
          onTap: onTap,
          splashColor: Colors.white60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: w * 0.4,
                padding: EdgeInsets.symmetric(vertical: 4),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(14),
                      topLeft: Radius.circular(14)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1.5,
                      blurRadius: 3,
                      offset: Offset(-1, 4), // changes position of shadow
                    ),
                  ],
                  color: purpleBrownies,
                ),
                child: (trigger ?? false)
                    ? Center(child: CircularProgressIndicator())
                    : Center(
                        child: Text(
                          firstText,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
              ),
              Container(
                width: w * 0.15,
                padding: EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                  color: Color(0xff0056FE),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1.5,
                      blurRadius: 3,
                      offset: Offset(-1, 4), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(14),
                      bottomRight: Radius.circular(14)),
                ),
                child: Center(
                    child: Text(
                  secondText,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 14),
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
