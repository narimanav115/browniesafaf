import 'package:brownies/ui/configs_themes/icons.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/widgets/appbar/buttons/bar_elements.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  CustomAppBar({Key key})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  Widget build(BuildContext context) {
    return AppBar(
      // leading: LeadingButton(),
      backgroundColor: Colors.white,
      elevation: 8,
      // centerTitle: true,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/title.png',
            scale: 4,
          ),
          SizedBox(
            width: 6,
          ),
          Icon(
            Brownies.pie,
            color: purpleBrownies,
          )
        ],
      ),
      actions: [
        Container(
          width: 40,
        )
      ],
    );
  }
}
