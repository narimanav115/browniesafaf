import 'package:brownies/ui/configs_themes/icons.dart';
import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:brownies/ui/screens/profile_page.dart';
import 'package:flutter/material.dart';

class LeadingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
                width: 40,
                height: 40,
                decoration: ShapeDecoration(
                  color: Colors.transparent,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                        width: 1.0, style: BorderStyle.solid, color: grey),
                    borderRadius: BorderRadius.all(Radius.circular(32)),
                  ),
                )),
            Icon(
              Brownies.back_arrow,
              color: purpleBrownies,
              size: 30,
            ),
          ],
        ),
      ),
    );
  }
}

class ProfileButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, ProfilePage.id);
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
                width: 40,
                height: 40,
                decoration: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                        width: 1.0,
                        style: BorderStyle.solid,
                        color: blueBrownies.withOpacity(0.75)),
                    borderRadius: BorderRadius.all(Radius.circular(32)),
                  ),
                )),
            Icon(
              Brownies.profile,
              color: blueBrownies.withOpacity(0.75),
              size: 30,
            ),
          ],
        ),
      ),
    );
  }
}
