import 'package:brownies/ui/configs_themes/theme.dart';
import 'package:flutter/material.dart';

class ContainerShadowed extends StatelessWidget {
  final List<Widget> children;
  final double width;
  final double height;
  final EdgeInsets margin;
  final bool rounded;
  final crossAxisAlignment;
  final mainAxisAlignment;
  const ContainerShadowed(
      {Key key,
      this.children,
      this.width,
      this.height,
      this.margin = const EdgeInsets.symmetric(horizontal: 28, vertical: 16),
      this.rounded = true,
      this.crossAxisAlignment = CrossAxisAlignment.start,
      this.mainAxisAlignment = MainAxisAlignment.spaceAround})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 14),
      height: height,
      width: width,
      margin: margin,
      decoration: decorationContainer,
      child: Column(
        mainAxisAlignment: mainAxisAlignment,
        crossAxisAlignment: crossAxisAlignment,
        children: children,
      ),
    );
  }
}
